<?php
require_once './includes/connection.php';
// require_once "./includes/functions.php";
if(!isset($_SESSION))
{
    session_start();
}
require_once './includes/functions.php';
if (!logged_in()) 
{
    // if (isset($_SESSION['admin'])) {
        header("Location: login.php");
        exit;
    // }
}
?>

<?php
require_once 'header.php';
?>

<?php
if (isset($_GET['page']) AND file_exists("includes/".$_GET['page'] . ".php")) 
	include_once "includes/".$_GET['page'] . ".php";
else
	include_once 'includes/dashboard.php';
?>


<?php
require_once 'footer.php';
?>

<?php
mysql_close();
?>