-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2016 at 10:02 PM
-- Server version: 5.6.15-log
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mercyeyedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE IF NOT EXISTS `attachment` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `file_desc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `patient_activity_f` int(11) DEFAULT NULL,
  `project_id_f` int(11) DEFAULT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted, but I prefer deleting the image permenantly',
  PRIMARY KEY (`a_id`),
  KEY `patient_activity_f` (`patient_activity_f`,`project_id_f`),
  KEY `project` (`project_id_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=94 ;

--
-- Dumping data for table `attachment`
--

INSERT INTO `attachment` (`a_id`, `filename`, `file_desc`, `uploaded_date`, `patient_activity_f`, `project_id_f`, `view`) VALUES
(21, 'clinic/20161122185252.png', 'clinics', '2016-11-22 15:52:52', 10, NULL, 1),
(22, 'clinic/20161122185252.png', 'clinics', '2016-11-22 15:52:52', 10, NULL, 1),
(23, 'clinic/20161122185252.png', 'clinics', '2016-11-22 15:52:52', 10, NULL, 1),
(24, 'clinic/20161122222911.png', 'clinics', '2016-11-22 19:29:11', 11, NULL, 1),
(26, 'clinic/20161122222913.png', 'clinics', '2016-11-22 19:29:13', 11, NULL, 1),
(39, 'clinic/2016112317251013.PNG', 'clinics', '2016-11-23 14:25:10', 12, NULL, 1),
(40, 'clinic/2016112317251023.JPG', 'clinics', '2016-11-23 14:25:10', 12, NULL, 1),
(41, 'clinic/2016112317251133.PNG', 'clinics', '2016-11-23 14:25:11', 12, NULL, 1),
(42, 'clinic/2016112317251143.png', 'clinics', '2016-11-23 14:25:11', 12, NULL, 1),
(43, 'surgery/2016112320571803.jpg', 'surgery', '2016-11-23 17:57:18', 13, NULL, 1),
(44, 'surgery/2016112320571813.jpg', 'surgery', '2016-11-23 17:57:18', 13, NULL, 1),
(46, 'surgery/2016112320580303.jpg', 'surgery', '2016-11-23 17:58:03', 2, NULL, 1),
(47, 'surgery/2016112321131403.jpg', 'surgery', '2016-11-23 18:13:14', 14, NULL, 1),
(48, 'surgery/2016112321183503.PNG', 'surgery', '2016-11-23 18:18:35', 2, NULL, 1),
(49, 'surgery/2016112321202903.PNG', 'surgery', '2016-11-23 18:20:29', 2, NULL, 1),
(50, 'surgery/2016112321223803.PNG', 'surgery', '2016-11-23 18:22:38', 2, NULL, 1),
(51, 'surgery/2016112321274303.PNG', 'surgery', '2016-11-23 18:27:43', 13, NULL, 1),
(52, 'surgery/2016112321321503.PNG', 'surgery', '2016-11-23 18:32:15', 13, NULL, 1),
(71, 'other projects/201611249493703.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(72, 'other projects/201611249493713.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(73, 'other projects/201611249493723.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(74, 'other projects/201611249493733.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(75, 'other projects/201611249493743.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(76, 'other projects/201611249493753.jpg', 'other projects', '2016-11-24 06:49:37', NULL, 5, 1),
(77, 'other projects/201611249493863.jpg', 'other projects', '2016-11-24 06:49:38', NULL, 5, 1),
(78, 'other projects/201611249493873.jpg', 'other projects', '2016-11-24 06:49:38', NULL, 5, 1),
(79, 'other projects/201611249493883.jpg', 'other projects', '2016-11-24 06:49:38', NULL, 5, 1),
(80, 'other projects/2016112410141503.jpg', 'other projects', '2016-11-24 07:14:15', NULL, 2, 1),
(81, 'other projects/2016112410141513.jpg', 'other projects', '2016-11-24 07:14:15', NULL, 2, 1),
(82, 'clinic/2016112518393105.jpg', 'clinics', '2016-11-25 15:39:31', 15, NULL, 1),
(83, 'clinic/2016112718154302.png', 'clinics', '2016-11-27 15:15:43', 11, NULL, 1),
(84, 'surgery/2016113012050403.jpg', 'surgery', '2016-11-30 09:05:04', 18, NULL, 1),
(85, 'surgery/2016113012050413.jpg', 'surgery', '2016-11-30 09:05:04', 18, NULL, 1),
(86, 'surgery/2016113012050423.jpg', 'surgery', '2016-11-30 09:05:04', 18, NULL, 1),
(87, 'clinic/2016120223185703.jpg', 'clinics', '2016-12-02 20:18:57', 19, NULL, 1),
(91, 'clinic/2016120314353603.jpg', 'clinics', '2016-12-03 11:35:36', 22, NULL, 1),
(92, 'clinic/2016120314353713.jpg', 'clinics', '2016-12-03 11:35:37', 22, NULL, 1),
(93, 'surgery/2016120419434502.jpg', 'surgery', '2016-12-04 16:43:45', 24, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT 'branch name: erbil, sulaimani, etc... ',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`b_id`, `name`, `view`) VALUES
(1, 'Erbil', 1),
(2, 'Sulaimania', 1),
(3, 'Duhok', 1);

-- --------------------------------------------------------

--
-- Table structure for table `disease`
--

CREATE TABLE IF NOT EXISTS `disease` (
  `d_id` int(11) NOT NULL AUTO_INCREMENT,
  `disease` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, 0:deleted',
  PRIMARY KEY (`d_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `disease`
--

INSERT INTO `disease` (`d_id`, `disease`, `view`) VALUES
(1, 'kidney Crystal', 1),
(2, 'exima', 1),
(3, 'headache', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `specialization` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doc_id`, `name`, `tel`, `address`, `specialization`, `view`) VALUES
(1, 'Hawbash Jawad', '123', 'roonaki', 'kedney', 1),
(2, 'muhammad nzar', '123', 'gulan', 'Batni', 1),
(3, 'Gashaw Aziz', '0750123456', 'Erbil', 'woman', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE IF NOT EXISTS `hospital` (
  `h_id` int(11) NOT NULL AUTO_INCREMENT,
  `hospital_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`h_id`),
  UNIQUE KEY `hospital_id` (`hospital_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`h_id`, `hospital_id`, `name`, `tel`, `address`, `view`) VALUES
(1, '2020', 'Zheen', '123', 'koya street', 1),
(2, '2030', 'Rizgari', '123', '100m', 1),
(3, '1100', 'Rozhawa', '23115', 'mousel street', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient_activity`
--

CREATE TABLE IF NOT EXISTS `patient_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sex` tinyint(1) NOT NULL COMMENT '0:male, 1:female',
  `age` int(3) NOT NULL,
  `occupation` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `residency` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id_f` int(11) NOT NULL,
  `disease_id_f` int(11) NOT NULL,
  `doctor_id_f` int(11) NOT NULL,
  `date` date NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1:clinic, 2:surgery',
  `hospital_id_f` int(11) DEFAULT NULL,
  `surgery_type_f` int(11) DEFAULT NULL,
  `amount_of_money` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`id`),
  KEY `disease_id_f` (`disease_id_f`,`doctor_id_f`,`hospital_id_f`),
  KEY `doctor_id_f` (`doctor_id_f`),
  KEY `hospital_id_f` (`hospital_id_f`),
  KEY `user_id_f` (`user_id_f`),
  KEY `branch_id_f` (`branch_id_f`),
  KEY `surgery_type_f` (`surgery_type_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `patient_activity`
--

INSERT INTO `patient_activity` (`id`, `fullname`, `sex`, `age`, `occupation`, `residency`, `branch_id_f`, `disease_id_f`, `doctor_id_f`, `date`, `type`, `hospital_id_f`, `surgery_type_f`, `amount_of_money`, `note`, `user_id_f`, `last_updated`, `view`) VALUES
(2, 'Omar Othman', 0, 88, 'Shopkeeper', 'Erbil', 1, 1, 1, '2016-11-23', 2, 1, 2, '2000000', 'a surgery', 3, '2016-11-23 18:18:35', 1),
(3, 'fatima hazm', 1, 45, 'house wife', 'sulaimani', 2, 2, 2, '2016-11-24', 1, 2, NULL, NULL, 'a new note', 4, '2016-11-19 10:14:33', 1),
(4, 'anwar gharib', 0, 47, 'no oc', 'sulaimani', 2, 2, 2, '2016-11-24', 1, 2, NULL, NULL, 'a new note', 2, '2016-11-19 08:04:20', 1),
(10, 'test name', 0, 25, 'any', 'any', 1, 2, 2, '2016-11-22', 1, NULL, NULL, NULL, 'any note', 3, '2016-11-23 11:39:25', 1),
(11, 'ahmad muhammad ahmad', 0, 40, 'teacher', 'erbil', 1, 1, 1, '2016-11-22', 1, NULL, NULL, NULL, 'some notes', 2, '2016-11-27 15:15:43', 1),
(12, 'Amina Aziz', 1, 59, 'Housewife', 'Sulaimani', 2, 2, 2, '2016-11-22', 1, NULL, NULL, NULL, 'some notes here', 3, '2016-11-23 12:46:48', 1),
(13, 'araz hasan', 0, 88, 'Shopkeeper', 'Erbil', 1, 1, 1, '2016-11-23', 2, 1, 2, '2000000', 'a surgery', 3, '2016-11-23 18:19:17', 1),
(14, 'masud fariq', 0, 45, 'any', 'sulaimani', 2, 2, 2, '2016-11-15', 2, 2, 1, '4500000', '', 3, '2016-11-23 19:11:11', 1),
(15, 'aram mustafa', 0, 88, 'test', 'Erbil', 1, 3, 3, '2016-11-25', 1, NULL, NULL, NULL, 'anything to write', 5, '2016-11-25 14:43:32', 1),
(16, 'test', 0, 39, 'test', 'test', 1, 2, 3, '2016-11-25', 1, NULL, NULL, NULL, '', 5, '2016-11-26 19:55:06', 1),
(17, 'karwan muhammad ali', 0, 15, 'student', 'daratw', 3, 2, 3, '2016-11-27', 2, 1, 1, '0', 'fjbsugfu', 2, '2016-11-27 15:20:48', 1),
(18, 'test test', 1, 77, 'test', 'any', 3, 1, 1, '2016-11-23', 2, 1, 1, '1200000', '', 3, '2016-11-30 09:05:04', 1),
(19, 'test mobile', 0, 56, 'any', 'any', 3, 2, 3, '2016-12-08', 1, NULL, NULL, NULL, '', 3, '2016-12-02 20:18:57', 1),
(20, 'test iphone', 0, 67, 'any', 'erbil', 1, 3, 2, '2016-11-30', 1, NULL, NULL, NULL, 'some notes', 3, '2016-12-03 11:04:46', 1),
(21, 'aram qadir', 0, 84, 'any', 'sulaimani', 2, 1, 1, '2016-11-24', 1, NULL, NULL, NULL, 'anything', 3, '2016-12-03 11:34:22', 1),
(22, 'hemn amin', 0, 34, 'Shopkeeper', 'sulaimani', 2, 1, 1, '2016-11-30', 1, NULL, NULL, NULL, '', 3, '2016-12-03 11:35:36', 1),
(23, 'Omar Othman', 0, 88, 'Shopkeeper', 'Erbil', 1, 1, 1, '2016-11-23', 2, 1, 2, '2000000', 'a surgery', 3, '2016-11-23 18:18:35', 1),
(24, 'anyname', 0, 65, 'anyoc', 'erbil', 1, 2, 3, '2016-12-14', 2, 2, 1, '45655666555', '', 2, '2016-12-04 16:43:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `leader` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `members` text COLLATE utf8_unicode_ci NOT NULL,
  `money` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `branch_id_f` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id_f` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`p_id`),
  KEY `attachment_id_f` (`user_id_f`),
  KEY `branch_id_f` (`branch_id_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`p_id`, `name`, `place`, `leader`, `members`, `money`, `date`, `branch_id_f`, `note`, `user_id_f`, `last_updated`, `view`) VALUES
(1, 'Orphan', 'Erbil', 'Hawbash Jawad', 'Hawbash Jawad, Gashaw Aziz, Nawras Nzar, Hawry Jawad', '500000', '2016-11-24', 1, 'we did an activity', 3, '2016-11-24 07:06:03', 1),
(2, 'Cancer', 'Sulaimani', 'Hawry Jawad', '1. Hawry Jawad\r\n2. Gashaw Aziz\r\n3. Helin Jawad', '450000', '2016-11-01', 2, 'some projects', 3, '2016-11-24 07:13:01', 1),
(3, 'Cancer', 'Sulaimani', 'Hawry Jawad', '1. Hawry Jawad\r\n2. Gashaw Aziz\r\n3. Helin Jawad', '300000', '2016-11-01', 2, 'some projects', 3, '2016-11-24 07:53:29', 1),
(4, 'Cancer', 'Sulaimani', 'Hawry Jawad', '1. Hawry Jawad\r\n2. Gashaw Aziz\r\n3. Helin Jawad', '300000', '2016-11-01', 2, 'some projects', 3, '2016-11-24 07:06:03', 1),
(5, 'Cancer', 'Sulaimani', 'Hawry Jawad', '1. Hawry Jawad\r\n2. Gashaw Aziz\r\n3. Helin Jawad', '300000', '2016-11-01', 2, 'some projects', 3, '2016-11-24 07:06:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `surgery_type`
--

CREATE TABLE IF NOT EXISTS `surgery_type` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `surgery` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`s_id`),
  UNIQUE KEY `surgery` (`surgery`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `surgery_type`
--

INSERT INTO `surgery_type` (`s_id`, `surgery`, `view`) VALUES
(1, 'heart', 1),
(2, 'kidney', 1),
(3, 'Jyub', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id_f` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:normal user, 9:admin',
  `view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active, -1:deleted',
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `username` (`username`),
  KEY `f_branch_id` (`branch_id_f`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `fullname`, `username`, `password`, `branch_id_f`, `role`, `view`) VALUES
(2, 'Nawras sulaimani', 'nawras', '5ae207fdc1969c31a8120707cde39c9f57474353', 2, 9, 1),
(3, 'Nawras Hawler', 'nawras1', 'ac53823e5757db513a91a388fe596aac3c07f606', 1, 9, 1),
(4, 'akam slemani', 'akam', '5ae207fdc1969c31a8120707cde39c9f57474353', 2, 9, 1),
(5, 'normal user', 'normal', '1684efb7429dafe6c4bc38325b8e1f25804a6073', 1, 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attachment`
--
ALTER TABLE `attachment`
  ADD CONSTRAINT `attachment_ibfk_1` FOREIGN KEY (`project_id_f`) REFERENCES `project` (`p_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `attachment_ibfk_2` FOREIGN KEY (`patient_activity_f`) REFERENCES `patient_activity` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `patient_activity`
--
ALTER TABLE `patient_activity`
  ADD CONSTRAINT `patient_activity_ibfk_1` FOREIGN KEY (`disease_id_f`) REFERENCES `disease` (`d_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_activity_ibfk_2` FOREIGN KEY (`doctor_id_f`) REFERENCES `doctor` (`doc_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_activity_ibfk_3` FOREIGN KEY (`hospital_id_f`) REFERENCES `hospital` (`h_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_activity_ibfk_4` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_activity_ibfk_5` FOREIGN KEY (`branch_id_f`) REFERENCES `branch` (`b_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `patient_activity_ibfk_6` FOREIGN KEY (`surgery_type_f`) REFERENCES `surgery_type` (`s_id`) ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`user_id_f`) REFERENCES `user` (`u_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `project_ibfk_2` FOREIGN KEY (`branch_id_f`) REFERENCES `branch` (`b_id`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`branch_id_f`) REFERENCES `branch` (`b_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
