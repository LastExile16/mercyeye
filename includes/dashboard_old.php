
<script type="text/javascript" src="js/Chart.bundle.min.js"></script>

<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="student-table col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-line-chart'></span> Reports
			</div>
		</div>
		<div class="panel-body">
			<div class="col-md-4 nn">
				<?php /*echo $bar->render();*/ ?>
			</div>
			<div class="col-md-4">
				<canvas id="activity_per_branch" height="400" width="400"></canvas>
			</div>
			<div class="col-md-4">
				<canvas id="phpactivity_per_branch" height="400" width="400"></canvas>
			</div>
		</div>
	</div>
	</div>
	
</div>

<script>

	var modern = {
        labels: ["Erbil", "Duhok", "Sulaimani"],
        datasets: [
	        {
	            label: 'Clinics',
	            data: [12, 19, 3],
	            backgroundColor: [
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(255, 99, 132, 0.2)',
	                'rgba(255, 99, 132, 0.2)'
	                ],
	                borderColor: [
	                'rgba(255,99,132,1)',
	                'rgba(255,99,132,1)',
	                'rgba(255,99,132,1)'
	                ],
	                borderWidth: 1
	        },

	        {
	        	label: 'Surgery',
	            data: [18, 10, 5],
				backgroundColor: [
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(75, 192, 192, 0.2)',
	                'rgba(75, 192, 192, 0.2)'
	            ],
			    borderColor: [
			        'rgba(75, 192, 192, 1)',
			        'rgba(75, 192, 192, 1)',
			        'rgba(75, 192, 192, 1)'
			    ],
			    borderWidth: 1
	        },
	        {
	        	label: 'Other Projects',
	            data: [2, 7, 15],
				backgroundColor: [
	                'rgba(5, 12, 192, 0.2)',
	                'rgba(5, 12, 192, 0.2)',
	                'rgba(5, 12, 192, 0.2)'
	            ],
			    borderColor: [
			        'rgba(75, 192, 192, 1)',
			        'rgba(75, 192, 192, 1)',
			        'rgba(75, 192, 192, 1)'
			    ],
			    borderWidth: 1
	        }
    	]
    };

	
	</script>

	<script>

var ctx = document.getElementById("activity_per_branch");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: modern,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>


<script type="text/javascript">
	$(document).ready(function() {
		var data = 'activity_per_branch=1' ;
		$.ajax(
		{
		   type: "POST",
		   url: "./includes/reports_old.php",
		   data: data,
		   cache: false,
		
		   success: function(data1)
		   {
		   		$(".nn").html(data1);
		   },
		   
		   statusCode: 
		   {
				404: function() {
				alert('ERROR 404 \npage not found');
				}
		   }

		});
		//////
	});
</script>