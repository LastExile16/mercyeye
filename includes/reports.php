<?php
require_once 'connection.php';
require_once 'functions.php';

if (isset($_POST['activity_per_branch'])) {
	//clinics
	$query = "SELECT b.name branch, count(id) counter, 'clinics' label
				FROM patient_activity p, branch b 
				WHERE p.view=1 AND b.view=1 AND type=1 AND branch_id_f = b.b_id
				GROUP BY b.name ORDER BY b.b_id";
	$clinics_result = mysql_query($query) or die(mysql_error() . "100");



$arrDatasets=array();
$arrDatasets[0] = array(
		"label"=>"clinics",
		"data" => array(),
		"backgroundColor" => array('rgba(255, 99, 132, 0.7)', 'rgba(255, 99, 132, 0.7)', 'rgba(255, 99, 132, 0.7)'),
		"borderColor" => array('rgba(255,99,132,1)','rgba(255,99,132,1)', 'rgba(255,99,132,1)'),
		"borderWidth" => 1
			);
$arrDatasets[1] = array(
		"label"=>"surgery",
		"data" => array(),
		"backgroundColor" => array('rgba(75, 192, 192, 0.7)', 'rgba(75, 192, 192, 0.7)', 'rgba(75, 192, 192, 0.7)'),
		"borderColor" => array('rgba(75, 192, 192, 1)','rgba(75, 192, 192, 1)', 'rgba(75, 192, 192, 1)'),
		"borderWidth" => 1
			);
$arrDatasets[2] = array(
		"label"=>"other projects",
		"data" => array(),
		"backgroundColor" => array('rgba(5, 12, 192, 0.7)', 'rgba(5, 12, 192, 0.7)', 'rgba(5, 12, 192, 0.7)'),
		"borderColor" => array('rgba(175, 192, 129, 1)','rgba(175, 192, 192, 1)', 'rgba(175, 192, 129, 1)'),
		"borderWidth" => 1
			);

	// $data_clinics = array();
/*when other branches added, you should add them here too*/
	$data_clinics = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);
	//loop through the returned data
	while($row = mysql_fetch_assoc($clinics_result)) {
		// $data_clinics[] = $row['counter'];
		foreach($data_clinics as $k=>$v){
		  if($k==$row['branch']){
		      $data_clinics[$k] = $row["counter"];
		      break;
		    }
		}
	}
	/*foreach ($data_clinics as $value) {
		$arrDatasets[0]["data"][]= $value;
	}*/
		$arrDatasets[0]["data"]= array_values($data_clinics);



	//surgery
	$query = "SELECT b.name branch, count(id) counter, 'surgery' label
					FROM patient_activity p, branch b 
					WHERE p.view=1 AND b.view=1 AND type=2 AND branch_id_f = b.b_id
					GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "101");

		// $data_surgery = array();
		//loop through the returned data
		$data_surgery = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);
		while($row = mysql_fetch_assoc($result)) {
			// $data_surgery[] = $row['counter'];
			foreach($data_surgery as $k=>$v){
				if($k==$row['branch']){
					$data_surgery[$k] = $row["counter"];
					break;
				}
			}

		}

		/*foreach ($data_surgery as $value) {
			$arrDatasets[1]["data"][]= $value;
		}*/
		$arrDatasets[1]["data"]= array_values($data_surgery);

	//other projects
	$query = "SELECT b.name branch, count(p_id) counter, 'other projects' label
					FROM project p, branch b 
					WHERE p.view=1 AND b.view=1 AND branch_id_f = b.b_id
					GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "102");

		// $data_other = array();

		$data_other = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);

		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			// $data_other[] = $row['counter'];
			foreach($data_other as $k=>$v){
				if($k==$row['branch']){
					$data_other[$k] = $row["counter"];
					break;
				}
			}
		}

		/*foreach ($data_other as $value) {
			$arrDatasets[2]["data"][]= $value;
		}*/
			// $arrDatasets[2]["data"][]= array_values($data_other); //wrong chunka yak elementi data dabtawa array
			$arrDatasets[2]["data"]= array_values($data_other);


		$arrLabels = array("Erbil", "Duhok", "Sulaimani");

		$arrReturn[0] = array('labels' => $arrLabels, 'datasets' => $arrDatasets);



		// PIE CHART
        
		$arrDatasets2 = array(array(
            "data"=> array(),
             "backgroundColor"=> [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            "hoverBackgroundColor"=> [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ])
        );


		//query to get data
		$data = array('Erbil'=>0,'Duhok'=>0,'Sulaimania'=>0);
		
		$query = "SELECT b.name branch, count(id) counter
				FROM patient_activity p, branch b 
				WHERE p.view=1 AND b.view=1 AND branch_id_f = b.b_id
				GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "103");

		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			foreach($data as $k=>$v){
				if($k==$row['branch']){
					$data[$k] = $row["counter"];
					break;
				}
			}
		}



		$query = "SELECT b.name branch, count(p_id) counter
					FROM project p, branch b 
					WHERE p.view=1 AND b.view=1 AND branch_id_f = b.b_id
					GROUP BY b.name ORDER BY b.b_id";
		$result = mysql_query($query) or die(mysql_error() . "104");

		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			foreach($data as $k=>$v){
				if($k==$row['branch']){
					$data[$k] = intval($data[$k])+intval($row["counter"]);
					break;
				}
			}
		}

		$arrDatasets2[0]["data"]= array($data["Erbil"], intval($data["Duhok"]), $data["Sulaimania"]);
		
// print_r($arrDatasets2);



		$arrLabels2 = array("Erbil", "Duhok", "Sulaimania");
		$arrReturn[1] = array('labels' => $arrLabels2, 'datasets' => $arrDatasets2);


	//line chart
		$query = 
    $arrDatasets3 = array(
			"label" => "Erbil", 
			"fill" => false, 
			"lineTension" => 0, 
			"backgroundColor" => "rgba(75,192,192,0.4)", 
			"borderColor" => "rgba(75,192,192,1)", 
			"pointBorderColor" => "rgba(75,192,192,1)", 
			"pointBorderWidth" => 5, 
			"pointHoverRadius" => 7, 
			"pointHoverBackgroundColor" => "rgba(75,192,192,1)", 
			"pointHoverBorderColor" => "rgba(220,220,220,1)", 
			"pointHoverBorderWidth" => 2, 
			"pointRadius" => 1, 
			"pointHitRadius" => 10, 
			"data" => array(5, 7, 9, 1, 0, 2, 9)
    	);
    $m = ['jan','feb', 'apr', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];




	$arrLabels = array("January", "February", "March", "April", "May", "June", "July");
	//now print the data
	print json_encode($arrReturn);
}

?>