<style type="text/css">
	
	.box{
		border: 1px solid gray;
		margin: 5px;
		height: 9em;
		width: 9em;
		float: left;
		border-radius: 4px;
		background-color: #FD7364;
		box-shadow: 2px 2px 4px #020101;

	}
	span.text {
		display: table-cell;
		vertical-align: middle;
		line-height: normal;
		height: 9em;
		text-align: center;
		width: 9em;
		color: white;
		font-weight: bold; 
	}

	span.text:hover {
		color: Yellow;
	}

	.box:hover {
		background-color: #FC2F3D;
		-webkit-transition: background-color 1s;
	    -moz-transition:    background-color 1s;
	    -ms-transition:     background-color 1s;
	    -o-transition:      background-color 1s;
	    transition:         background-color 1s;
	}

	.box:active {
		box-shadow: none;
		position: relative;
		  top: 3px;
		  margin-bottom: 0;

		/*-webkit-transition: box-shadow 1s;*/
	    /*-moz-transition:    box-shadow 1s;*/
	    /*-ms-transition:     box-shadow 1s;*/
	    /*-o-transition:      box-shadow 1s;*/
	    /*transition:         box-shadow 1s;*/
	}

</style>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>

<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="caption panel-title">
					<span class='fa fa-cogs'></span> System Settings
				</div>
			</div>

			<div class="container-fluid">
				<div class="row">
					<div class="r3">
						
					<div class="box">
					<a href="javascript:;"><span class="text">change data 1</span></a>
					</div>
					<div class="box">
					<a href="javascript:;"><span class="text">change data 2</span></a>
					</div>
					<div class="box">
					<a href="javascript:;"><span class="text">change data 3</span></a>
					</div>
					<div class="box">
					<a href="javascript:;"><span class="text">change data 4</span></a>
					</div>
					<div class="box">
					<a href="javascript:;"><span class="text">change data 5</span></a>
					</div>
					<div class="box">
					<a href="javascript:;"><span class="text">change data 6</span></a>
					</div>
					
				
					</div>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>
	</div>
</div>