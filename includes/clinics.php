<script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php 
	echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
	<div class='form-group errmsj row' style='display:none'>
		<div class='col-md-12'>
			<div class='alert alert-danger alert-dismissable'>
			<button type='button' class='close' aria-hidden='true'>&times; </button>
				{$_GET['err_msj']}
			</div>
		</div>
	</div>":""; 
?>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-stethoscope'></span> Clinics
			</div>
		</div>
		<div class="panel-body flip-scroll table-responsive">
	        
			<table id="clinics" class="table-bordered table-condensed flip-content selection">
			
			<thead class="flip-content">
			<tr>
				<th>
					 #
				</th>
				<th>
					Name
				</th>
				<th>
					 Sex
				</th>
				<th>
					 Age
				</th>
				<th>
					 Occupation
				</th>
				<th>
					 Residency
				</th>
				<th>
					 Disease
				</th>
				<th>
					 Doctor
				</th>
				<th>
					 Date
				</th>
				<th>
					 Entered By
				</th>
				<?php 
				echo is_admin()?"<th>Branch: </th>":"";
				 ?>
				<th >
					 tools
				</th>
			</tr>
			</thead>

			<tfoot>
			<tr>
				<th>
					 #
				</th>
				<th>
					Name
				</th>
				<th>
					 Sex
				</th>
				<th>
					 Age
				</th>
				<th>
					 Occupation
				</th>
				<th>
					 Residency
				</th>
				<th>
					 Disease
				</th>
				<th>
					 Doctor
				</th>
				<th>
					 Date
				</th>
				<th>
					 Entered By
				</th>
				<?php 
				echo is_admin()?"<th>Branch: </th>":"";
				 ?>
				<th >
					 tools
				</th>
			</tr>
			</tfoot>
			<tbody>

			<?php
				$side_counter = 1;
				
				$output = "";
				$modal = "";
				// get_patient_activity($from=-1, $count=-1, $deleted=0, $type=1, $fromDate=NULL, $toDate=NULL)

				if (isset($_POST['search']) && ((isset($_POST['fromDate']) && !empty($_POST['fromDate'])) || (isset($_POST['toDate']) && !empty($_POST['toDate']) ) )){
					$clinic_set = get_patient_activity(-1, -1, 0, 1, $_POST['fromDate'], $_POST['toDate']);

				}else{
					$clinic_set = get_patient_activity(-1, -1, 0, 1);
				}

				while ($clinic = mysql_fetch_assoc($clinic_set)) {
					$sex = $clinic['sex']==0?'male':'female';

					$output .= "<tr id='{$clinic['id']}' class='clickable-row'>
						<td>
							{$side_counter}
						</td>
						<td>
							{$clinic['fullname']}
						</td>
						<td>";
							 $output .= $sex;
						$output .= "</td>
						<td>
							 {$clinic['age']}
						</td>
						<td>
							{$clinic['occupation']}
						</td>
						<td >
							 {$clinic['residency']}
						</td>
						<td>
							 {$clinic['disease']}
						</td>
						<td>
							 {$clinic['doc_name']}
						</td>
						<td>
							 {$clinic['date']}
						</td>
						<td>
							 {$clinic['entry_user']}
						</td>";
				$output .= is_admin()?"<td>{$clinic['branch']}</td>":"";

$edit_permission = has_edit_permission($clinic['id']);

$modal_edit = $edit_permission?"<a href='index.php?page=clinics_setup&act=2&clinic_id={$clinic['id']}' class='btn default purple'><i class='fa fa-edit'></i> edit </a>":"";
$edit_n_delete_button = $edit_permission?"<a href='javascript:;' role='button' class='btn default btn-xs red delete_activity'>
				<i class='fa fa-trash-o'></i> delete </a>
			<a href='index.php?page=clinics_setup&act=2&clinic_id={$clinic['id']}' class='btn default btn-xs purple'>
				<i class='fa fa-edit'></i> edit </a>":"";


			$output .= "<td>
							<a class='btn default btn-xs blue' data-toggle='modal' href='#basic{$clinic['id']}'>
								<i class='fa fa-share'></i> view </a>
							 {$edit_n_delete_button}
						</td>
					</tr>";
					$side_counter++;


			$attachments_output = "no attachments";
			$attachment_set = get_attachment(1, $clinic['id']);
			if (mysql_num_rows($attachment_set)>0) {
			$attachments_output = "
			<div class='gallery row'>
				 <div class='container col-md-12'>
					 <h2>Attachments</h2>
					 <div class='gallery-bottom'>";
					 $i = 0;
					 $flag=true;
				while ( $attachment = mysql_fetch_assoc($attachment_set)) {
					//4 pics in a row (by creating a new gallery-1)
					if ($i % 4 == 0) {
						$attachments_output .=	"<div class='gallery-1'>";
						$flag = true;
					}
					$attachments_output .=	"<div class='col-md-3 gallery-grid'>
										<a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$clinic['id']}'><img class='example-image' src='uploads/{$attachment['filename']}' alt='image'/></a>
									</div>";
						// $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
					if (++$i % 4 == 0) {
						$attachments_output .= "<div class='clearfix'></div></div>";
						$flag = false;
					}
					// $i++;
				}

				//if <div gallery-1> was opened but not closed then close it. 
				if ($flag) {
					$attachments_output .= "<div class='clearfix'></div></div>";
				}
			$attachments_output .= "</div>
						 </div>
					</div>";

			}

			$branch_modal = is_admin()?"<div class='row'>
									<div class='col-md-4 modal_data'>Name: </div><div class='col-md-8 modal_data'>{$clinic['branch']}</div>
								</div>":"";

			$modal .= "
				<div class='modal fade' id='basic{$clinic['id']}' tabindex='-1' role='basic' aria-hidden='true'>
				<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='row'>
						<div class='modal-header col-md-12'>
							<h4 class='modal-title'>Patient Information</h4>
						</div>
					</div>
					<div class='row'>
						<div class='modal-body modal-body-scroll col-md-12'>
								
								<div class='row'>
									<div class='col-md-4 modal_data'>Full Name: </div><div class='col-md-8 modal_data'>{$clinic['fullname']}</div>
								</div>
								<div class='row'>
									<div class='col-md-4 modal_data'>Sex: </div><div class='col-md-8 modal_data'>{$sex}</div>
								</div>
								<div class='row'>
									<div class='col-md-4 modal_data'>Age: </div><div class='col-md-8 modal_data'>{$clinic['age']}</div>
								</div>
								<div class='row'>
									<div class='col-md-4 modal_data'>Occupation Number: </div><div class='col-md-8 modal_data'>{$clinic['occupation']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Residency: </div><div class='col-md-8 modal_data'>{$clinic['residency']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Disease: </div><div class='col-md-8 modal_data'>{$clinic['disease']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Doctor: </div><div class='col-md-8 modal_data'>{$clinic['doc_name']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Date: </div><div class='col-md-8 modal_data'>{$clinic['date']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Entered by: </div><div class='col-md-8 modal_data'>{$clinic['entry_user']}</div>
								</div>
								{$branch_modal}
								<div class='row'>
									<div class='col-md-4 modal_data'>Last Updated on: </div><div class='col-md-8 modal_data'>{$clinic['last_updated']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Note: </div><div class='col-md-8 modal_data'>{$clinic['note']}</div>
								</div>
								{$attachments_output}
						</div>
					</div>
					<div class='row'>
						<div class='modal-footer col-md-12'>
							<button type='button' class='btn default red' data-dismiss='modal'><i class='fa fa-times-square-o'></i> close</button>
							{$modal_edit}
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		";
				}
							
				echo $output;
				
			?>
		</tbody>
	</table>
	<button class="btn btn-danger tfoot_button">show column search</button>
	</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="index.php?page=clinics_setup&act=1" class="btn btn-primary"><i class="fa fa-plus-square"></i> add new activity</a>
		</div>
	</div>
	</div>
	
</div>
<?php 
echo $modal;
 ?>
<script src="js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script>
<script type="text/javascript">

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iMin = $('#min').val();
        var iMax = $('#max').val();
        d1 = new Date(iMin);
        d2 = new Date(iMax);

        iMin = isNaN(d1.getTime())?"":d1.getTime();
        iMax = isNaN(d2.getTime())?"":d2.getTime();

        target_col_data = aData[8];


        iDate = new Date(target_col_data).getTime();

 		// console.log("iMin = "+iMin);
        // console.log("iMax = "+iMax);
        // console.log("iDate = "+iDate);
        if ( iMin == "" && iMax == "" )
		{
			return true;
		}
		else if ( iMin == "" && iDate < iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && "" == iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && iDate <= iMax )
		{
			return true;
		}
        return false;

    }
);
	$(document).ready(function() {
		$(document).on('click', '.tfoot_button', function () {
			$('tfoot').slideToggle();
		});
		
		$(".errmsj").slideDown(1000);
		$(".alert button.close").click(function (e) {
		    // $(this).parent().fadeOut('slow');
		    $(this).parent().slideUp('slow');
		});

		

		//number of all columns
    // var numCols = $('#example').DataTable().columns().nodes().length;
	//number of visible columns
    // var numCols = $('#clinics thead th').length;

    var clinics = $('#clinics').DataTable( {
    	processing: true,
    	buttons: [
	        'pdf'
	    ],
    	"pagingType": "full_numbers",
    	"columnDefs": [
		    { "orderable": false, "targets": -1 },
		    { "searchable": false, "targets": -1 }
		  ],
		  "dom": '<"top row"<"col-md-4"l><"rangefilter"><"col-md-4"f>>rt<"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
            "thousands": ","
        },
        "deferRender": true
    } );//end datatable

    $("div.rangefilter").html("<div id='baseDateControl'><div class='col-md-4 dateControlBlock'>Between <input type='text' name='min' id='min' class='date datepick form-control input-sm' value='' size='8' /> and <input type='text' name='max' id='max' class='date datepick form-control input-sm' value='' size='8'/> <a href='javascript:;' class='btn default btn-sm blue' id='clear_search'>clear</a></div></div>");
	
// l - Length changing
// f - Filtering input
// t - The Table!
// i - Information
// p - Pagination
// r - pRocessing

    /*$('#min, #max').keyup( function() {
        clinics.draw();});*/

    $("#min").keyup ( function() { clinics.draw(); } );
	$("#min").change( function() { clinics.draw(); } );
	$("#max").keyup ( function() { clinics.draw(); } );
	$("#max").change( function() { clinics.draw(); } );

	$("#clear_search").on("click", function() {
		$('#min').val("");
		$('#max').val("");
		clinics.draw();
	});

	/*
	pewistm baw initilize a nia chunka classi "date"m bakar hinaya lo inputakan ka xoi initializish 
	dakat w jwan jwanish nishani dadat (taibata baw theme a)
	$('.datepick').datepicker(
			format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true
	);*/	
	// Setup - add a text input to each footer cell
    // $('#clinics tfoot th').each( function () {
    $('#clinics tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search here" />' );
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
        	// $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    clinics.columns().every( function () {
        var that = this;
        // console.log(this.index());
        if (this.index()==0 || this.index()==11 ) {return;}
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );

} );
</script>