<?php
require_once 'connection.php';
require_once 'functions.php';

require_once '../phpchartjs/vendor/autoload.php';
use Halfpastfour\PHPChartJS\Chart\Bar;

if (isset($_POST['activity_per_branch'])) {
	//clinics
	/*
		Array
(
    [branch] => Duhok
    [counter] => 1
    [label] => clinics
)
Array
(
    [branch] => Erbil
    [counter] => 5
    [label] => clinics
)
Array
(
    [branch] => Sulaimania
    [counter] => 5
    [label] => clinics
)
	*/
	$query = "SELECT b.name branch, count(id) counter, 'clinics' label
				FROM patient_activity p, branch b 
				WHERE p.view=1 AND type=1 AND branch_id_f = b.b_id
				GROUP BY b.name";
	$result = mysql_query($query) or die(mysql_error() . "100");
$bar = new Bar();
$bar->setId( 'myChart' );

// Set labels
$bar->getLabels()->exchangeArray( [ "Erbil", "Duhok", "Sulaimania" ] );

// Add Datasets
$clincis_bar = $bar->createDataSet();

$clincis_bar->setLabel( "clincis_bar" )
			->setBackgroundColor( "rgba( 0, 150, 0, .5 )" )
			->setborderColor( "rgba( 2, 150, 0, 1 )" )
			->setborderWidth( 2 )
			->data()->exchangeArray( [ 12, 19, 3 ] );
$bar->addDataSet( $clincis_bar );

$oranges = $bar->createDataSet();
$oranges->setLabel( "oranges" )
	->setBackgroundColor( 'rgba( 255, 153, 0, .5 )' )
	->data()->exchangeArray( [ 30, 29, 5 ] );
$bar->addDataSet( $oranges );
echo $bar->render();

	$data = array();

	//loop through the returned data
	while($row = mysql_fetch_assoc($result)) {
		$data[] = $row;
	}
	
	//surgery
	$query = "SELECT b.name branch, count(id) counter, 'surgery' label
					FROM patient_activity p, branch b 
					WHERE p.view=1 AND type=2 AND branch_id_f = b.b_id
					GROUP BY b.name";
		$result = mysql_query($query) or die(mysql_error() . "101");


		//loop through the returned data
		while($row = mysql_fetch_assoc($result)) {
			$data[] = $row;
		}
	//other projects
	$query = "SELECT b.name branch, count(p_id) counter, 'other' label
					FROM project p, branch b 
					WHERE p.view=1 AND branch_id_f = b.b_id
					GROUP BY b.name";
		$result = mysql_query($query) or die(mysql_error() . "102");

}




		//loop through the returned data
		// while($row = mysql_fetch_assoc($result)) {
		// 	$data[] = $row;
		// }

	//now print the data
	// print json_encode($data);
?>