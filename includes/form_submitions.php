<?php

require_once './connection.php';
// require_once "./functions.php";
if(!isset($_SESSION))
{
    session_start();
}
require_once './functions.php';
if (!logged_in()) 
{
    // if (isset($_SESSION['admin'])) {
        header("Location: login.php");
        exit();
    // }
}
//  add\edit clinics activity
if (isset($_POST['add_edit_clinics']) && isset($_GET['act']) && is_numeric($_GET['act']) && isset($_POST['branch_id']) ) {
	// check branch data integrity to make sure it hasn't compromised via inspect element
	$is_admin = is_admin();
	if ( $is_admin || $_POST['branch_id'] == $_SESSION['branch']) {

		//check data permission
		if ($_GET['act']==1 || ($is_admin || ($_GET['act']==2 && has_edit_permission($_POST['clinic_id']))) ) {
			
/*
	1: add
	2: edit
*/
	/*echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";*/
	
        /*
        INSERT INTO patient_activity (id, fullname, sex, age, occupation, residency, branch_id_f, disease_id_f, doctor_id_f, date, type, hospital_id_f, surgery_type_f, amount_of_money, note, user_id_f, last_updated, view) VALUES (NULL, 'hameed shexa', '0', '32', 'farmer', 'Erbil', '1', '2', '2', '2016-11-21', '1', NULL, NULL, NULL, 'a note for this patient', '3', CURRENT_TIMESTAMP, '1');
        */

		$fullname = safe(trim($_POST['fullname']));
		$sex = safe(trim($_POST['sex']));
		$age = safe(trim($_POST['age']));
		$occupation = safe(trim($_POST['occupation']));
		$residency = safe(trim($_POST['residency']));
		$branch_id = safe(trim($_POST['branch_id']));
		$disease_id = safe(trim($_POST['disease_id']));
		$doctor_id = safe(trim($_POST['doctor_id']));
		$date = safe(trim($_POST['date']));
		$note = safe(trim($_POST['note']));

		//if ADD
		if ($_GET['act']==1) {
			$query = "INSERT INTO patient_activity (fullname, sex, age, occupation, residency, branch_id_f, disease_id_f, doctor_id_f, date, type, note, user_id_f, view) 
			VALUES 
			('{$fullname}', {$sex}, {$age}, '{$occupation}', '{$residency}', {$branch_id}, {$disease_id}, {$doctor_id}, '{$date}', '1', '$note', {$_SESSION['user_id']}, '1')";
			mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
			$last_inserted_or_updated_ID = mysql_insert_id();
		}
		//else if EDIT
		elseif ($_GET['act']==2) {
			$clinic_id = safe(trim($_POST['clinic_id']));

			$query = "UPDATE patient_activity SET 
			fullname = '{$fullname}',
			sex = {$sex},
			age = {$age},
			occupation = '{$occupation}',
			residency = '{$residency}',
			branch_id_f = {$branch_id},
			disease_id_f = {$disease_id},
			doctor_id_f = {$doctor_id},
			date = '{$date}',
			note = '$note',
			user_id_f = {$_SESSION['user_id']}
			WHERE id = {$clinic_id}
			";
			mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
			$last_inserted_or_updated_ID = $clinic_id;
		}

	// upload_file($errmsj, $uploading_files, $folder='', $query_part_1, $query_part_2)
	$err_msj="";
	$query_part1 = "INSERT INTO attachment(filename, file_desc, patient_activity_f) VALUES
							(";
	$query_part2 = ", 'clinics', {$last_inserted_or_updated_ID})";

	$err_msj = upload_file($err_msj, $_FILES, 'clinic', $query_part1, $query_part2);


		} //end if check data permission
	} // end of BRANCH DATA INTEGRITY

	// echo "<meta http-equiv='refresh' content='0;url=index.php?page=clinics&err_msj={$err_msj}' />";

		header('Location: ../index.php?page=clinics&err_msj=' . $err_msj);
		exit();
}//end ADD\EDIT CLINICS

//********************************************************************//
//  add\edit Surgery activity
if (isset($_POST['add_edit_surgery']) && isset($_GET['act']) && is_numeric($_GET['act']) && isset($_POST['branch_id']) ) {
	// check branch data integrity to make sure it hasn't compromised via inspect element
	$is_admin = is_admin();
	if ( $is_admin || $_POST['branch_id'] == $_SESSION['branch']) {

		//check data permission
		if ($_GET['act']==1 || ($is_admin || ($_GET['act']==2 && has_edit_permission($_POST['surgery_id']))) ) {
			
/*
	1: add
	2: edit
*/
	/*echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";*/
	
        /*
        INSERT INTO patient_activity (id, fullname, sex, age, occupation, residency, branch_id_f, disease_id_f, doctor_id_f, date, type, hospital_id_f, surgery_type_f, amount_of_money, note, user_id_f, last_updated, view) VALUES (NULL, 'hameed shexa', '0', '32', 'farmer', 'Erbil', '1', '2', '2', '2016-11-21', '1', NULL, NULL, NULL, 'a note for this patient', '3', CURRENT_TIMESTAMP, '1');
        */

		$fullname = safe(trim($_POST['fullname']));
		$sex = safe(trim($_POST['sex']));
		$age = safe(trim($_POST['age']));
		$occupation = safe(trim($_POST['occupation']));
		$residency = safe(trim($_POST['residency']));
		$branch_id = safe(trim($_POST['branch_id']));
		$disease_id = safe(trim($_POST['disease_id']));
		$doctor_id = safe(trim($_POST['doctor_id']));
		$date = safe(trim($_POST['date']));
		$hospital_id_f = safe(trim($_POST['hospital_id']));
		$surgery_type_f = safe(trim($_POST['surgery_type_id']));
		$amount_of_money = safe(trim($_POST['money']));
		$note = safe(trim($_POST['note']));

		//if ADD
		if ($_GET['act']==1) {
			$query = "INSERT INTO patient_activity (fullname, sex, age, occupation, residency, branch_id_f, disease_id_f, doctor_id_f, date, type, hospital_id_f, surgery_type_f, amount_of_money, note, user_id_f, view) 
			VALUES 
			('{$fullname}', {$sex}, {$age}, '{$occupation}', '{$residency}', {$branch_id}, {$disease_id}, {$doctor_id}, '{$date}', '2', {$hospital_id_f}, {$surgery_type_f}, '{$amount_of_money}', '$note', {$_SESSION['user_id']}, '1')";
			mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
			$last_inserted_or_updated_ID = mysql_insert_id();
		}
		//else if EDIT
		elseif ($_GET['act']==2) {
			$surgery_id = safe(trim($_POST['surgery_id']));

			$query = "UPDATE patient_activity SET 
			fullname = '{$fullname}',
			sex = {$sex},
			age = {$age},
			occupation = '{$occupation}',
			residency = '{$residency}',
			branch_id_f = {$branch_id},
			disease_id_f = {$disease_id},
			doctor_id_f = {$doctor_id},
			date = '{$date}',
			hospital_id_f = {$hospital_id_f},
			surgery_type_f = {$surgery_type_f},
			amount_of_money = {$amount_of_money},
			note = '$note',
			user_id_f = {$_SESSION['user_id']}
			WHERE id = {$surgery_id}
			";
			mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
			$last_inserted_or_updated_ID = $surgery_id;
		}
			// upload_file($errmsj, $uploading_files, $folder='uploads', $query_part_1, $query_part_2)
			$err_msj="";
			$query_part1 = "INSERT INTO attachment(filename, file_desc, patient_activity_f) VALUES
									(";
			$query_part2 = ", 'surgery', {$last_inserted_or_updated_ID})";

			$err_msj = upload_file($err_msj, $_FILES, 'surgery', $query_part1, $query_part2);

		} //end if check data permission
	} // end of BRANCH DATA INTEGRITY

	// echo "<meta http-equiv='refresh' content='0;url=index.php?page=surgery&err_msj={$err_msj}' />";

		header('Location: ../index.php?page=surgery&err_msj=' . $err_msj);
		exit();
}//end ADD\EDIT SURGERY

// **************************************************************************
// Add\Edit other projects
if (isset($_POST['add_edit_other_proj']) && isset($_GET['act']) && is_numeric($_GET['act']) && isset($_POST['branch_id']) ) {
/*
	1: add
	2: edit
*/
	/*echo "<pre>";
	print_r($_POST);
	print_r($_FILES);
	echo "</pre>";*/
	
	/*
		[name] => Orphan
	    [place] => Erbil
	    [leader] => Hawbash Jawad
	    [members] => Hawbash Jawad, Gashaw Aziz, Nawras Nzar, Hawry Jawad
	    [money] => 500000
	    [date] => 2016-11-24
	    [branch_id] => 1
	    [note] => we did an activity
	    if edit: [other_proj_id]
    */


	$is_admin = is_admin();
	if ( $is_admin || $_POST['branch_id'] == $_SESSION['branch']) {

		//check data permission
		if ($_GET['act']==1 || ($is_admin || ($_GET['act']==2 && has_edit_permission($_POST['other_proj_id']))) ) {

			$name = safe(trim($_POST['name']));
			$place = safe(trim($_POST['place']));
			$leader = safe(trim($_POST['leader']));
			$members = safe(trim($_POST['members']));
			$money = safe(trim($_POST['money']));
			$date = safe(trim($_POST['date']));
			$branch_id = safe(trim($_POST['branch_id']));
			$note = safe(trim($_POST['note']));

			//if ADD
			if ($_GET['act']==1) {
				$query = "INSERT INTO project(name, place, leader, members, money, date, branch_id_f, note, user_id_f) VALUES
				('{$name}', '{$place}', '{$leader}', '{$members}', '{$money}', '{$date}', {$branch_id}, '{$note}', {$_SESSION['user_id']})";
				mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
				$last_inserted_or_updated_ID = mysql_insert_id();
			}
			//else if EDIT
			elseif ($_GET['act']==2) {
				$other_proj_id = safe(trim($_POST['other_proj_id']));

				$query = "UPDATE project SET 
				name = '{$name}',
				place = '{$place}',
				leader = '{$leader}',
				members = '{$members}',
				money = '{$money}',
				date = '{$date}',
				branch_id_f = {$branch_id},
				note = '{$note}',
				user_id_f = {$_SESSION['user_id']}
				WHERE p_id = {$other_proj_id}
				";
				mysql_query($query) or die(mysql_error() . ' \nthe data is not clean, please re-enter or contact system administrator.');
				$last_inserted_or_updated_ID = $other_proj_id;
			}

			// upload_file($errmsj, $uploading_files, $folder='uploads', $query_part_1, $query_part_2)
			$err_msj="";
			$query_part1 = "INSERT INTO attachment(filename, file_desc, project_id_f) VALUES
									(";
			$query_part2 = ", 'other projects', {$last_inserted_or_updated_ID})";
			$err_msj = upload_file($err_msj, $_FILES, 'other projects', $query_part1, $query_part2);
		} //end if check data permission

	} // end of BRANCH DATA INTEGRITY

	echo $err_msj;

// echo "<meta http-equiv='refresh' content='0;url=index.php?page=surgery&err_msj={$err_msj}' />";

		header('Location: ../index.php?page=others&err_msj=' . $err_msj);
		exit();
}

//chane profile info
if (isset($_POST['change_profile'])) {
	
	$err_msj="";
	$clean_to_change_pass=false;
	$fullname = safe(trim($_POST['fullname']));
	$username = safe(trim($_POST['username']));
	if (isset($_POST['changepass'])) {
		if (isset($_POST['old_password']) && !empty($_POST['old_password']) &&
			isset($_POST['new_password']) && !empty($_POST['new_password']) &&
			isset($_POST['new_password_2']) && !empty($_POST['new_password_2'])

			) {
		
			$old_pass = safe(trim($_POST['old_password']));
			$old_pass = sha1(md5($old_pass));
			$new_pass = safe(trim($_POST['new_password']));
			$new_pass2 = safe(trim($_POST['new_password_2']));
			if ($new_pass != $new_pass2) {
				header('Location: ../index.php?page=my_profile&err_msj=new password does not match!');
				exit();
			}
			
			$DB_old_pass=mysql_query("SELECT password FROM user WHERE u_id={$_SESSION['user_id']} AND username='{$_SESSION['name']}' AND password='{$old_pass}' LIMIT 1") or die("wrong user credentials");
			
			if (mysql_num_rows($DB_old_pass)==1) {
				$new_pass = sha1(md5($new_pass));
				$clean_to_change_pass = true;
			}
		}
	}

	if (isset($_POST['changepass']) && !$clean_to_change_pass) {
		header('Location: ../index.php?page=my_profile&err_msj=Wrong old password!');
		exit();
	}
	$query = "UPDATE user SET
				fullname = '{$fullname}',
				username = '{$username}' ";
	$query .= isset($_POST['changepass']) && $clean_to_change_pass?" , password='{$new_pass}'":" ";

	$query .= " WHERE u_id = {$_SESSION['user_id']}";
// echo $query;
	if (!mysql_query($query)) {
		$errmsj = "failed to update user data, please choose another username.";
	}
	else{
		$_SESSION['fullname'] = $fullname;
		$_SESSION['name'] = $username;
	}
	header('Location: ../index.php?page=my_profile&err_msj=' . $errmsj);
	exit();
}

?>