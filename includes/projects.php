<style type="text/css">
	
	.team{
		animation-duration: 1.2s;	
	-webkit-animation-duration: 1.2s;

	}

	.ch-grid *{
		cursor: pointer;
	}
</style>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="team expandUp">
		<div class="team-top heading">
				<h3>MercyEye Projects</h3>
		</div>
	 <div class="team-bottom">
			<ul class="ch-grid">
					<li><a href="index.php?page=clinics">
						<div class="ch-item ch-img-1">				
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-1"></div>
									<div class="ch-info-back">
										<h3>Clinics</h3>
										<p>Add New Clinics Activity</p>
									</div>	
								</div>
							</div>
						</div>
						</a>
					</li>

					<li>
						<a href="index.php?page=surgery">
						<div class="ch-item ch-img-2">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-2"></div>
									<div class="ch-info-back">
										<h3>Surgery</h3>
										<p>Add New surgery Activity</p>
									</div>
								</div>
							</div>
						</div>
						</a>
					</li>
					<li>
						<a href="index.php?page=others">
						<div class="ch-item ch-img-3">
							<div class="ch-info-wrap">
								<div class="ch-info">
									<div class="ch-info-front ch-img-3"></div>
									<div class="ch-info-back">
										<h3>Other Projects</h3>
										<p>Add New Project Data</p>
									</div>
								</div>
							</div>
						</div>
						</a>
					</li>
			</ul>
	 </div>
</div>

	</div>
	<div class="col-md-2">

	</div>
	
</div>
