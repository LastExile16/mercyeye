<?php
//check if user logged in
function logged_in(){
	//return isset($_SESSION['user_id']);

   if(isset($_SESSION['user_id'], $_SESSION['name'], $_SESSION['login_string'])) 
   {
		$user_id = $_SESSION['user_id'];
		$user_name = $_SESSION['name'];
		$login_string = $_SESSION['login_string'];
		$user_browser = $_SERVER['HTTP_USER_AGENT']; if($user_name == "Admin"){ return true; } $result = mysql_query("SELECT password FROM user WHERE u_id='$user_id' LIMIT 1"); $result = mysql_fetch_array($result); $login_check = hash('sha512', $result['password'].$user_browser);
		if($login_check == $login_string)
		{
			return true;
		}
		else
		{
			return false;
		}
   }
   else
	{
		return false;
	}
}

//generate random salt
function generateSalt($max = 4 , $temp = 0) 
{
	if($temp == 0)
	{
		$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@$%&";
	}
	else
	{
		$characterList = "0123456789";
	}
	$i = 0;
	$salt = "";
	do {
		$salt .= $characterList{mt_rand(0,strlen($characterList)-1)};
		$i++;
	} while ($i <= $max);
	return $salt;
}
//verify the input before inserting to a database
function safe($value)
{
	$magic_quotes_active = get_magic_quotes_gpc();
	$new_enough_php = function_exists("mysql_real_escape_string"); //i.e. PHP >=v4.3.0
	if($new_enough_php){//PHP v4.3.0 or higher
	//undo any magic quote effects so mysql_real_escape_string can do the work 
	if($magic_quotes_active){ $value = stripcslashes($value); }
	$value = mysql_real_escape_string($value);
	}else{//before PHP v4.3.0
	//if magic quotes aren't already on then add slashes manually
	if(!$magic_quotes_active){ $value = addslashes($value); }
	//if magic quotes are active, then the slashes already exist
	}
	
	$value=htmlentities($value, ENT_QUOTES, "UTF-8");
	//$value=strip_tags($value, '<a><b>');
	return $value;
}

function is_admin()
{
	return $_SESSION['role']==9?true:false;

}

// upload files to the server
function upload_file($errmsj, $uploading_files, $folder='', $query_part_1, $query_part_2)
{
	$err_msj = $errmsj;
	/*echo "<pre>";
	echo print_r($uploading_files);*/
	// echo count($uploading_files['files']['name'])."<br />";
	// echo var_dump(count($uploading_files['files']['name']));
	$num_of_files = count($uploading_files['files']['name']);
	if($num_of_files>0 && !empty($uploading_files['files']['name'][0])) {
		// foreach ($uploading_files['files']['name'] as $file) {
		for ($i=0; $i <$num_of_files; $i++) { 

			// echo $file;
			if (($uploading_files["files"]["type"][$i] == "image/gif")
		      || ($uploading_files["files"]["type"][$i] == "image/jpeg")
		      || ($uploading_files["files"]["type"][$i] == "image/jpg")
		      || ($uploading_files["files"]["type"][$i] == "image/pjpeg")
		      || ($uploading_files["files"]["type"][$i] == "image/x-png")
		      || ($uploading_files["files"]["type"][$i] == "image/png")) 
			{
				if( $uploading_files['files']['error'][$i]>0)
				{
					switch($uploading_files['files']['error'][$i])
					{
						case 1:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> maximum size exceeded.<br />";
						break;
						case 3:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> The uploaded file was only partially uploaded.<br />";
						break;
						case 4:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> No file was uploaded.<br />";
						break;
						case 6:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> Missing a temporary folder. <br />";
						break;
						case 7:
						$err_msj .= "Error: " . $uploading_files["files"]["error"][$i] . "<br /> Failed to write file to disk. <br />";
						break;
						default:
						$err_msj .= "Error: unable to upload, please contact site administrator.<br />";
						break;
					}
				}
				else
				{
					//process
					date_default_timezone_set("Asia/Baghdad");
					$todaydate = date('Ymd');
					//we add trailer to Gis because we don't have microsecond so to avoid wrong overwrite
					// we will add file number and user session.
					$todayhours = date('Gis') . $i . $_SESSION['user_id'];
					$temp = explode(".",$uploading_files["files"]["name"][$i]);
					$newfilename_and_path = "{$folder}/" . $todaydate.$todayhours . '.' .end($temp);
					// echo "<pre>". getcwd().DIRECTORY_SEPARATOR ."</pre>";

					//write all data to the database
					// $err_msj .= getcwd().DIRECTORY_SEPARATOR;
					// $err_msj .= "../uploads/" . $newfilename_and_path;
					if(move_uploaded_file($uploading_files["files"]["tmp_name"][$i], "../uploads/" . $newfilename_and_path))
					{
						/*$query = "INSERT INTO attachment(filename, file_desc, patient_activity_f) VALUES
									('{$newfilename_and_path}', 'surgery', {$last_inserted_or_updated_ID})";*/
							$query = $query_part_1 . "'{$newfilename_and_path}'" . $query_part_2;	
						// echo $query;
							// $err_msj = $query;

						// mysql_query($query) or die(mysql_error());
						if(!mysql_query($query)){
							$err_msj .= "activity has been added BUT attachments cannot be stored.";
						}
					}
					else
					{
						$err_msj .= "activity has been added BUT Error in moving attachments to correct directory.";
					}
					
				}
				// echo $err_msj;
			}
			else
			{
				$err_msj .= " wrong file type.";
			}
			/*
			if you don't want to give extra information just uncomment this:
			if (!empty($err_msj)) 
			{
				header('HTTP/1.1 500 Internal Server Error');
			}*/
		
			
		}
	} // end of FILE UPLOAD

	// echo $err_msj;
	return $err_msj;
}

//get Patient activity(Clinics or Surgery) according to the parameter
function get_patient_activity($from=-1, $count=-1, $deleted=0, $type=1, $fromDate=NULL, $toDate=NULL)
{
	// type=1 clinics
	
	if ($type == 1) {
		//FULL $query = "SELECT  p.id, p.fullname, p.sex, p.age, p.occupation, p.residency, p.branch_id_f, p.disease_id_f, p.doctor_id_f, p.date, p.note, p.user_id_f, p.last_updated p.view";

		$query = "SELECT  p.id, p.fullname, p.sex, p.age, p.occupation, p.residency, p.date, p.note, p.last_updated,"; 
		
		$query .= is_admin()?" b.name branch, ":"";
		$query .= " d.disease, 
		doc.name doc_name, 
		user.fullname entry_user";

		$query .= " FROM patient_activity p, ";
		$query .= is_admin()?" branch b, ":"";
		$query .= " disease d, doctor doc, user"; 

		$query .= $deleted==0?" WHERE p.view = 1 AND type={$type} ":" WHERE p.view = -1 AND type={$type} ";

		$query .= is_admin()?" AND p.branch_id_f = b_id":"";
		$query .= " AND p.disease_id_f = d_id AND p.doctor_id_f = doc_id AND p.user_id_f = user.u_id";
		$query .= is_admin()?"":" AND p.branch_id_f = {$_SESSION['branch']}";
	}
	// type=2 surgery
	else{
		$query = "SELECT  p.id, p.fullname, p.sex, p.age, p.occupation, p.residency, p.branch_id_f, p.disease_id_f, p.doctor_id_f, p.date, p.type, p.hospital_id_f, p.surgery_type_f, p.amount_of_money, p.note, p.user_id_f, p.last_updated, ";
		$query .= is_admin()?" b.name branch, ":"";

		$query .= "d.disease, 
		doc.name doc_name,
		h.name hospital,
		s.surgery, 
		user.fullname entry_user";

		$query .= " FROM patient_activity p,";
		$query .= is_admin()?" branch b, ":"";
		$query .=" disease d, doctor doc, hospital h, surgery_type s, user"; 

		$query .= $deleted==0?" WHERE p.view = 1 AND type = {$type}":" WHERE p.view = -1 AND type = {$type}";

		$query .= is_admin()?" AND p.branch_id_f = b_id":"";
		$query .= " AND p.disease_id_f = d_id AND p.doctor_id_f = doc_id AND p.hospital_id_f = h_id AND p.surgery_type_f = s_id AND p.user_id_f = user.u_id";
		$query .= is_admin()?"":" AND p.branch_id_f = {$_SESSION['branch']}";
	}

	if (!is_null($fromDate) && !is_null($toDate)) {
		$query .= " AND (p.date between $fromDate AND $toDate)";
	}

	$query .= " ORDER BY p.date";
	if ($from >-1) {
		$query .= " LIMIT $from, $count";
	}
	// echo $query;
	
	$activity_set = mysql_query($query) or die("Q1_1".mysql_error());
	return $activity_set;
}

function get_other_projects($from=-1, $count=-1, $deleted=0, $fromDate=NULL, $toDate=NULL)
{
	/*
		FULL $query = SELECT p_id, name, place, leader, members, money, branch_id_f, user_id_f, view
	*/

	$query = "SELECT o.p_id, o.name, o.place, o.leader, o.members, o.money, o.date, o.branch_id_f, o.note, o.user_id_f, o.last_updated, ";
	$query .= is_admin()?" b.name branch, ":"";
	$query .= " user.fullname entry_user";

	$query .= " FROM project o,";
	$query .= is_admin()?" branch b, ":"";
	$query .=" user";

	$query .= $deleted==0?" WHERE o.view = 1 ":" WHERE o.view = -1 ";
	$query .= is_admin()?" AND o.branch_id_f = b_id":"";
	$query .= " AND o.user_id_f = user.u_id";
	$query .= is_admin()?"":" AND o.branch_id_f = {$_SESSION['branch']}";

	if (!is_null($fromDate) && !is_null($toDate)) {
		$query .= " AND (o.date between $fromDate AND $toDate)";
	}

	$query .= " ORDER BY o.date";
	if ($from >-1) {
		$query .= " LIMIT $from, $count";
	}
	// echo $query;
	
	$other_projects_set = mysql_query($query) or die("Q1_2".mysql_error());
	return $other_projects_set;
}

//get attached pictures to clinics, surgery and projects
// 1: patient_activity (clinic+surgery)
// 2: projects
function get_attachment($type, $id)
{
	$query = "SELECT a_id, filename, file_desc, uploaded_date";
	$query .= " FROM attachment ";
	$query .= " WHERE view=1";
	$query .= $type==1?" AND patient_activity_f={$id}":" AND project_id_f={$id}";
	$query .= " ORDER BY uploaded_date";
	$attachment_set = mysql_query($query) or die("Q2".mysql_error());
	return $attachment_set;
}

//get all/one branch name(s)
function getBranch($id=0)
{
	$query = "SELECT b_id, name";
	$query .= " FROM branch ";
	$query .= " WHERE view=1";
	$query .= $id!=0?" AND b_id={$id}":"";
	$query .= " ORDER BY name";
	$branch_set = mysql_query($query) or die("Q3".mysql_error());
	return $branch_set;
}

//get all/one disease name(s)
function getDisease($id=0)
{
	$query = "SELECT d_id, disease";
	$query .= " FROM disease ";
	$query .= " WHERE view=1";
	$query .= $id!=0?" AND d_id={$id}":"";
	$query .= " ORDER BY disease";
	$disease_set = mysql_query($query) or die("Q4".mysql_error());
	return $disease_set;
}

//get all/one doctor name(s)
function getDoctor($id=0)
{
	$query = "SELECT doc_id, name, tel, address, specialization";
	$query .= " FROM doctor ";
	$query .= " WHERE view=1";
	$query .= $id!=0?" AND doc_id={$id}":"";
	$query .= " ORDER BY name";
	$doctor_set = mysql_query($query) or die("Q5".mysql_error());
	return $doctor_set;
}

//get all/one hospital name(s)
function getHospital($id=0)
{
	
	$query = "SELECT h_id, hospital_id, name, tel, address";
	$query .= " FROM hospital";
	$query .= " WHERE view=1";
	$query .= $id!=0?" AND h_id={$id}":"";
	$query .= " ORDER BY name";
	$hospital_set = mysql_query($query) or die("Q6".mysql_error());
	return $hospital_set;
}

//get all/one surgery name(s)
function getSurgeryType($id=0)
{
	
	$query = "SELECT s_id, surgery";
	$query .= " FROM surgery_type";
	$query .= " WHERE view=1";
	$query .= $id!=0?" AND s_id={$id}":"";
	$query .= " ORDER BY surgery";
	$surgery_set = mysql_query($query) or die("Q7".mysql_error());
	return $surgery_set;
}


// return activity edit permission for current user
function has_edit_permission($clinic_id)
{
	if (is_admin($_SESSION['user_id'])) {
		return true;
	}
	else{
		$query = "SELECT id FROM patient_activity";
		$query .= " WHERE id={$clinic_id} AND user_id_f={$_SESSION['user_id']} AND view=1 LIMIT 1";
		if (mysql_num_rows(mysql_query($query))>0) {
			return true;
		}else{
			return false;
		}
	}
}

// return other edit permission for current user
function has_edit_permission_other($other_proj)
{
	if (is_admin($_SESSION['user_id'])) {
		return true;
	}
	else{
		$query = "SELECT p_id FROM project";
		$query .= " WHERE p_id={$other_proj} AND user_id_f={$_SESSION['user_id']} AND view=1 LIMIT 1";
		if (mysql_num_rows(mysql_query($query))>0) {
			return true;
		}else{
			return false;
		}
	}
}
// change date format to date format in output
function FormcorrectDate($ddate)
{
	$newdate="";
	//ddate = 2016-02-24 to 24-02-2016
	if (!empty($ddate)) {
		$ddate=explode("-",$ddate);
		$y=$ddate[0];
		$m=$ddate[1];
		$d=$ddate[2];
		$newdate="$d-$m-$y";
	}
	
	
	return $newdate;
}

// USER CONTROL
function get_users()
{
	$query = "SELECT u_id, full_name, username, role, view
				FROM `user`";
	$user_set = mysql_query($query) or die("Q1_usrcntrl ". mysql_error());
	return $user_set;
}

?>