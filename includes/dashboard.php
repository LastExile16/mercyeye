<script type="text/javascript" src="js/Chart.bundle.min.js"></script>

<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="student-table col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-line-chart'></span> Reports
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<canvas id="canvas" height="400" width="400"></canvas>
				</div>
				<div class="col-md-6">
					<canvas id="phpactivity_per_branch" height="300" width="400"></canvas>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-5 col-md-offset-5">
					<canvas id="activity_per_branch" height="300" width="400"></canvas>
				</div>
			</div>
		</div>
	</div>
	</div>
	
</div>

<script>
var randomScalingFactor = function(){ return Math.round(Math.random()*20)};
/*var PieChart = 
			{

				labels: ["Erbil", "Duhok", "Sulaimani"],
				datasets:[
			        {
			            data: ["20", randomScalingFactor(), randomScalingFactor()],
			            backgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#FFCE56"
			            ],
			            hoverBackgroundColor: [
			                "#FF6384",
			                "#36A2EB",
			                "#FFCE56"
			            ]
			        }
			    ]
			};*/
// console.table(PieChart);
		
		

	/*var linechartdata = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "Erbil",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        },
        {
            label: "Duhok",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(243, 30, 38, 0.4)",
            borderColor: "rgba(243, 30, 38, 1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(243, 30, 38, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(243, 30, 38, 1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
            spanGaps: false,
        },
        {
            label: "Sulaimania",
            fill: false,
            lineTension: 0,
            backgroundColor: "rgba(197, 127, 255, 0.4)",
            borderColor: "rgba(197, 127, 255, 1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(197, 127, 255, 1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 5,
            pointHoverRadius: 7,
            pointHoverBackgroundColor: "rgba(197, 127, 255, 1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()],
            spanGaps: false,
        }
    ]
};



var ctx = document.getElementById("activity_per_branch");
var myChart = new Chart(ctx, {
    type: 'line',
    data: linechartdata,
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});*/
</script>


<script type="text/javascript">
	$(document).ready(function() {
		var data = 'activity_per_branch=1' ;
		$.ajax(
		{
		   type: "POST",
		   url: "./includes/reports.php",
		   data: data,
		   cache: false,
		
		   success: function(data1)
		   {

				// console.log(data1);
				var r_data = JSON.parse(data1);
				console.table(r_data[1]);
				
				
				var ctx = document.getElementById("phpactivity_per_branch");
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: r_data[0],
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				                    beginAtZero:true
				                }
				            }]
				        }
				    }
				});

				var ctx2 = document.getElementById("canvas");
				var myChart2 = new Chart(ctx2, {
			    type: 'pie',
			    data: r_data[1],
			        options: {
				        tooltips: {
				            callbacks: {
				                label: function(tooltipItem, data) {
				                    var allData = data.datasets[tooltipItem.datasetIndex].data;
				                    var tooltipLabel = data.labels[tooltipItem.index];
				                    var tooltipData = allData[tooltipItem.index];
				                    var total = 0;
				                    for (var i in allData) {
				                        total += allData[i];
				                    }
				                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
				                    return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
				                }
				            }
				        }
				    }
			});



		   },
		   
		   statusCode: 
		   {
				404: function() {
				alert('ERROR 404 \npage not found');
				}
		   }

		});
		//////
	});
</script>