<?php 
//FULL $query = SELECT id, fullname, sex, age, occupation, residency, branch_id_f, disease_id_f, doctor_id_f, date, type, hospital_id_f, surgery_type_f, amount_of_money, note, user_id_f, last_updated, view
/*
1: add
2: edit
*/
$surgery_id = 0;
$attachments_output = "";
if (isset($_GET['act']) && is_numeric($_GET['act']) ) {
	if ($_GET['act']==1 || ($_GET['act']==2 && isset($_GET['surgery_id']) && is_numeric($_GET['surgery_id']))) {
		
		//if act is edit then start initialization
	if ($_GET['act']==2) {
		$surgery_id = $_GET['surgery_id'];
		//each normal user can only edit their own entered data
		if (!has_edit_permission($surgery_id)) {
			exit(100);
		}
		$query = "SELECT  p.id, p.fullname, p.sex, p.age, p.occupation, p.residency, p.branch_id_f, p.disease_id_f, p.doctor_id_f, p.date, p.hospital_id_f, p.surgery_type_f, p.amount_of_money, p.note, p.user_id_f
		From patient_activity p WHERE id=$surgery_id AND view=1 LIMIT 1";
		$surgery_set = mysql_query($query) or die("wrong data");
		if (mysql_num_rows($surgery_set)>0) {
			//retrive record data
			$surgery = mysql_fetch_assoc($surgery_set);

			//retrive file attachments
			$attachments_output = "no attachments";
						$attachment_set = get_attachment(1, $surgery['id']);
						if (mysql_num_rows($attachment_set)>0) {
						$attachments_output = "
						<div class='gallery gallery-border row'>
							 <div class='container col-md-12'>
								 <div class='gallery-bottom'>";
								 $i = 0;
								 $flag=true;
							while ( $attachment = mysql_fetch_assoc($attachment_set)) {
								//4 pics in a row (by creating a new gallery-1)
								if ($i % 4 == 0) {
									$attachments_output .=	"<div class='gallery-1'>";
									$flag = true;
								}
								$attachments_output .=	"<div class='col-md-3 gallery-grid'>
												<div class='img-wrap'><span data-id='{$attachment['a_id']}' file-name='{$attachment['filename']}' id='close_{$attachment['a_id']}' class='close delete-image'>&times;</span>
													<a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$surgery['id']}'>
														<img class='example-image' src='uploads/{$attachment['filename']}' alt='image'/>
													</a>
													</div>
												</div>";
									// $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
								if (++$i % 4 == 0) {
									$attachments_output .= "<div class='clearfix'></div></div>";
									$flag = false;
								}
								// $i++;
							}

							//if <div gallery-1> was opened but not closed then close it. 
							if ($flag) {
								$attachments_output .= "<div class='clearfix'></div></div>";
							}
						$attachments_output .= "</div>
									 </div>
								</div>";

						}
		}else{
			exit(101);
		}
		// print_r($surgery);
	}
?>
<style type="text/css">
	.gallery{
		padding: 0px;
		margin-top: -10px;
		margin-bottom: 15px;
	}
	.gallery-1{
		margin-top: 5px;
	}
	.gallery-grid{
		padding-left: 5px;
		padding-right: 5px;
	}
	.gallery-border{
		border: 1px dashed #337AB7;
		padding-bottom: 5px;
	}
	.input-group {
    	margin-bottom: 0px;
	}
	.img-wrap {
	    position: relative;
	    border: 1px solid #d1cfcf;
		border-radius: 4px;
	}
	.img-wrap .close {
	    position: absolute;
	    top: -3px;
	    right: 2px;
	    z-index: 100;
	}
	.example-image{
		border-radius: 4px;
	}

	.close{
		color: red;
		opacity: 0.4;
	}
	.close:focus, .close:hover {
    	color: #F30909;
		background-color: white;
		border-style: solid;
		border-width: 1px;
		border-color: gray;
		border-top-width: 0px;
		box-shadow: 1px 1px 2px -1px red, -1px -1px 2px -1px red; 
		opacity: 0.8;
	}



</style>
<script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
<div class="contact row">
	<div class="add-panel col-md-7">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-user-plus"></i> Add New Surgery Activity</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" role="form" enctype="multipart/form-data" action="./includes/form_submitions.php?to=surgery<?php echo "&act={$_GET['act']}" ?>" method="POST" onsubmit="return checkfiles()">
			<div class="form-group">
			   <label for="fullname" class="col-md-3 control-label">Full Name</label>
			   <div class="col-md-7">
				  <input type="text" required="required" class="form-control" id="fullname" name="fullname" value="<?php echo !empty($surgery['fullname'])?$surgery['fullname']:'' ?>">
			   </div>
			</div>
			<div class="form-group">
			   <label for="sex" class="col-md-3 control-label">Sex</label>
			   <div class="col-md-7">
					 <select class="form-control" id="sex" name="sex">
				  <option <?php echo !empty($surgery['sex'])&& $surgery['sex']==0?'selected':'' ?> value="0">Male</option>
				  <option <?php echo !empty($surgery['sex'])&& $surgery['sex']==1?'selected':'' ?> value="1">Female</option>
			   </select>
			   </div>
			</div>
			<div class="form-group">
			   <label for="firstname" class="col-md-3 control-label">Age</label>
			   <div class="col-md-7">
				  <input type="number" min="0" required="required" class="form-control" id="age" name="age" value="<?php echo !empty($surgery['age'])?$surgery['age']:'' ?>">
			   </div>
			</div>
			<div class="form-group">
			   <label for="occupation" class="col-md-3 control-label">Occupation</label>
			   <div class="col-md-7">
				  <input type="text" required="required" class="form-control" id="occupation" name="occupation" value="<?php echo !empty($surgery['occupation'])?$surgery['occupation']:'' ?>">
			   </div>
			</div>
			<div class="form-group">
			   <label for="residency" class="col-md-3 control-label">Residency</label>
			   <div class="col-md-7">
				  <input type="text" required="required" class="form-control" id="residency" name="residency" value="<?php echo !empty($surgery['residency'])?$surgery['residency']:'' ?>">
			   </div>
			</div>
			<?php 
			   if(is_admin()){
				 echo "<div class='form-group'>
					 <label for='branch_id' class='col-md-3 control-label'>Branch</label>
					 <div class='col-md-7'>
						   <select class='form-control' id='branch_id' name='branch_id' required='required'>";
						   
							  $branch_set = getBranch();
							  while ($branch = mysql_fetch_assoc($branch_set)) {
							  	$selected = !empty($surgery['branch_id_f']) && $branch['b_id'] == $surgery['branch_id_f']?'selected':'';
								 echo "<option {$selected} value='{$branch['b_id']}'>{$branch['name']}</option>";
							  }
					 echo "</select>
					 </div>
				  </div>";
			   }
			   else{
				  echo "<input type='hidden' name='branch_id' value={$_SESSION['branch']}>";
			   }
			  echo $_GET['act']==2?"<input type='hidden' name='surgery_id' value={$surgery['id']}>":"";
			?>
			<div class="form-group">
			   <label for="disease_id" class="col-md-3 control-label">disease</label>
			   <div class="col-md-7">
					 <select class="form-control" id="disease_id" name="disease_id" required="required">
					 <?php  
						$disease_set = getDisease();
						while ($disease = mysql_fetch_assoc($disease_set)) {
							$selected = !empty($surgery['disease_id_f']) && $disease['d_id'] == $surgery['disease_id_f']?'selected':'';
						   echo "<option {$selected} value='{$disease['d_id']}'>{$disease['disease']}</option>";
						}
					 ?>
			   </select>
			   </div>
			</div>
			<div class="form-group">
			   <label for="doctor_id" class="col-md-3 control-label">Doctor</label>
			   <div class="col-md-7">
					<select class="form-control" id="doctor_id" name="doctor_id" required="required">
					 <?php  
						$doctor_set = getDoctor();
						while ($doctor = mysql_fetch_assoc($doctor_set)) {
							$selected = !empty($surgery['doctor_id_f']) && $doctor['doc_id'] == $surgery['doctor_id_f']?'selected':'';
								echo "<option {$selected} value='{$doctor['doc_id']}'>{$doctor['name']}, {$doctor['specialization']}</option>";
						}
					 ?>
					</select>
			   </div>
			</div>
			<div class="form-group">
			   <label for="datetimepicker1" class="col-md-3 control-label">Date</label>
			   <div class="col-md-7">
				  <div class='input-group date datepick' id='datetimepicker1'>
				  <input type='text' class="form-control" name="date" required="required" value="<?php echo !empty($surgery['date'])?$surgery['date']:'' ?>" />
				  <span class="input-group-addon">
					 <span class="glyphicon glyphicon-calendar"></span>
				  </span>
					 </div>
			   </div>
			</div>

			<div class="form-group">
			   <label for="hospital_id" class="col-md-3 control-label">Hospital</label>
			   <div class="col-md-7">
					<select class="form-control" id="hospital_id" name="hospital_id" required="required">
					 <?php  
						$hospital_set = getHospital();
						while ($hospital = mysql_fetch_assoc($hospital_set)) {
							$selected = !empty($surgery['hospital_id_f']) && $hospital['h_id'] == $surgery['hospital_id_f']?'selected':'';
								echo "<option {$selected} value='{$hospital['h_id']}'>{$hospital['hospital_id']}, {$hospital['name']}</option>";
						}
					 ?>
					</select>
			   </div>
			</div>

			<div class="form-group">
			   <label for="surgery_type_id" class="col-md-3 control-label">SurgeryType</label>
			   <div class="col-md-7">
					<select class="form-control" id="surgery_type_id" name="surgery_type_id" required="required">
					 <?php  
						$surgery_type_set = getSurgeryType();
						while ($surgery_type = mysql_fetch_assoc($surgery_type_set)) {
							$selected = !empty($surgery['surgery_type_f']) && $surgery_type['s_id'] == $surgery['surgery_type_f']?'selected':'';
								echo "<option {$selected} value='{$surgery_type['s_id']}'>{$surgery_type['surgery']}</option>";
						}
					 ?>
					</select>
			   </div>
			</div>

			<div class="form-group">
		        <label for="c1" class="col-md-3 control-label">Amount of Money</label>
		        <div class="col-md-7">
		        <div class='input-group' id='c1'>
			        <input type="number" min="0" class="form-control" id="currency" name="money" required="required" value="<?php echo !empty($surgery['amount_of_money'])?$surgery['amount_of_money']:''; ?>" />
			        <span class="input-group-addon">$</span>
			    </div>
			    </div>
		    </div>

		    

			<div class="form-group">
			   <label for="attachment" class="col-md-3 control-label">Attachments</label>
			   <div class="col-md-7">
				  <div class="input-group">
					  <label class="input-group-btn">
						  	<span class="btn btn-primary">
							  Browse&hellip; <input type="file" id="attachfiles" name="files[]" accept="image/gif,image/jpeg,image/jpg,image/pjpeg,image/png,image/x-png, .jpeg" style="display: none;" multiple="multiple">
							  
							</span>
					  </label>
					  <input type="text" class="form-control" readonly="readonly" disabled="disabled">
				  </div>
					  <?php  
					  	
						echo $attachments_output;
					  ?>
			   </div>
			</div>
			<div class="form-group">
			   <label for="note" class="col-md-3 control-label">Notes</label>
			   <div class="col-md-7">
				  <textarea class="form-control" id="note" placeholder="max: 65,535 characters" name="note" rows="4" cols="50"><?php echo !empty($surgery['note'])?$surgery['note']:'' ?></textarea>
			   </div>
			</div>

			<div class="form-group">
			   <div class=" col-md-9">
				  <button type="submit" name="add_edit_surgery" class="btn btn-info actionbutton"><?php echo $_GET['act']==1?"Add Activity":"Update Activity" ?></button>
				  <a href="index.php?page=surgery" class="btn btn-warning actionbutton">Cancel</a>
			   </div>
			</div>
		 </form>
			</div>
		</div>
	</div>
</div>

<script src="js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script>
<script type="text/javascript">
   
   // $(function() {
// from https://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3
  // We can attach the `fileselect` event to all file inputs on the page

  // We can watch for our custom `fileselect` event like this
$(document).ready( function() {

	// file selection
	$(document).on('change', ':file', function() {
	var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
	});
	$(':file').on('fileselect', function(event, numFiles, label) {

		var input = $(this).parents('.input-group').find(':text'),
		  log = numFiles > 1 ? numFiles + ' files selected' : label;
		if( input.length ) {
		  input.val(log);
		  // console.log(log);
		} 
		else {
		  if( log ) alert(log);
		}
	});//end of file selection

	// image delete
	$('.delete-image').click(function()
	{
		if (confirm(" Are you sure you want to delete this image?"))
		{
			var id = $(this).data('id');
			var filename = $(this).attr('file-name');
			console.log(id);
			var data = 'image_delete_id=' + id +'&filename='+filename;
			var parent = $(this).parent().parent();

			$.ajax(
			{
			   type: "POST",
			   url: "./includes/delete_attachment.php",
			   data: data,
			   cache: false,
			
			   success: function(data1)
			   {
			   		if (data1=="") {
						parent.fadeOut('slow', function() {$(this).remove();});
			   		}else{
					alert(data1);
			   		}
			   },
			   
			   statusCode: 
			   {
					404: function() {
					alert('ERROR 404 \npage not found');
					}
			   }

			});
		}				
		
	}); //end of image delete
});

  
// }); //end of $(function())

   function checkfiles() {
    //check whether browser fully supports all File API
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
    	var flag_type = true;
    	var flag_size = true;

        //get the file size and file type from file input field
        for (var i = $('#attachfiles')[0].files.length - 1; i >= 0; i--) {
        	// $('#attachfiles')[0].files[i];
	        var fsize = $('#attachfiles')[0].files[i].size;
	        var ftype = $('#attachfiles')[0].files[i].type;
	        var fname = $('#attachfiles')[0].files[i].name;
	       
	       switch(ftype)
	        {
	            case 'image/png':
	            case 'image/gif':
	            case 'image/jpeg':
	            case 'image/pjpeg':
	                // alert("Acceptable image file!");
	                break;
	            default:
	                // alert('Unsupported File!');
	                flag_type = false;
	                break;
	        }
	        //check file size
	        // if(fsize>5242880) //do something if file size more than 5 mb (5242880)
	        if(fsize>15242880)
	        {
	            flag_size = false;
	        }

	        if (flag_type && flag_size) {
	        	return true;
	        }else if(!flag_type){
	        	alert('Unsupported File!');
	        	return false;
	        }else if(!flag_size){
	        	alert(fsize +' \nfile size Too big!');
	        	return false;
	        }

        }
    }else{
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    }
}
</script>

<?php
	} //end of EDIT OR ADD IF
}//end of First IF
?>