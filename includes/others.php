<script type="text/javascript" src="js/modernizr.custom.79639.js"></script>
<div class="contact row">

	<!-- BEGIN SEARCH TABLE PORTLET-->
	
<!-- END SEARCH TABLE PORTLET-->
	<div class="col-md-2">
	</div>
</div>
<?php 
	echo isset($_GET['err_msj']) && !empty($_GET['err_msj'])? "
	<div class='form-group errmsj row' style='display:none'>
		<div class='col-md-12'>
			<div class='alert alert-danger alert-dismissable'>
			<button type='button' class='close' aria-hidden='true'>&times; </button>
				{$_GET['err_msj']}
			</div>
		</div>
	</div>":""; 
?>
<div class="row">
	 
	 <!-- BEGIN SAMPLE TABLE PORTLET-->
	 <div class="col-md-12">
	 	
	 
	<div class="panel panel-info">
		<div class="panel-heading">
			<div class="caption panel-title">
				<span class='fa fa-users'></span> Other Projects
			</div>
		</div>
		<div class="panel-body flip-scroll table-responsive">
	        
			<table id="other_proj" class="table-bordered table-condensed flip-content selection">
			
			<thead class="flip-content">
			<tr>
				<th>
					 #
				</th>
				<th>
					Name
				</th>
				<th>
					 Place
				</th>
				<th>
					 Leader
				</th>
				<th>
					 Members
				</th>
				<th>
					 Amount of Money
				</th>
				<th>
					 Date
				</th>
				<th>
					 Entered By
				</th>
				<?php 
				echo is_admin()?"<th>Branch: </th>":"";
				 ?>
				<th >
					 tools
				</th>
			</tr>
			</thead>

			<tfoot>
			<tr>
				<th>
					 #
				</th>
				<th>
					Name
				</th>
				<th>
					 Place
				</th>
				<th>
					 Leader
				</th>
				<th>
					 Members
				</th>
				<th>
					 Amount of Money
				</th>
				<th>
					 Date
				</th>
				<th>
					 Entered By
				</th>
				<?php 
				echo is_admin()?"<th>Branch: </th>":"";
				 ?>
				<th >
					 tools
				</th>
			</tr>
			</tfoot>
			<tbody>

			<?php
				$side_counter = 1;
				
				$output = "";
				$modal = "";
				// get_other_projects($from=-1, $count=-1, $deleted=0, $fromDate=NULL, $toDate=NULL)

				if (isset($_POST['search']) && ((isset($_POST['fromDate']) && !empty($_POST['fromDate'])) || (isset($_POST['toDate']) && !empty($_POST['toDate']) ) )){
					$other_proj_set = get_other_projects(-1, -1, 0, $_POST['fromDate'], $_POST['toDate']);

				}else{
					$other_proj_set = get_other_projects();
				}

				while ($other_proj = mysql_fetch_assoc($other_proj_set)) {
					// selected data 
					// p_id, name, place, leader, members, money, date, branch_id_f, user_id_f, branch, entry_user,
 					$output .= "<tr id='{$other_proj['p_id']}' class='clickable-row'>
						<td>
							{$side_counter}
						</td>
						<td>
							{$other_proj['name']}
						</td>
						<td>
							 {$other_proj['place']}
						</td>
						<td>
							{$other_proj['leader']}
						</td>
						<td >
							 {$other_proj['members']}
						</td>
						<td>
							 {$other_proj['money']}
						</td>
						<td>
							 {$other_proj['date']}
						</td>
						<td>
							 {$other_proj['entry_user']}
						</td>";
				$output .= is_admin()?"<td>{$other_proj['branch']}</td>":"";

$edit_permission = has_edit_permission_other($other_proj['p_id']);
$modal_edit = $edit_permission?"<a href='index.php?page=others_setup&act=2&other_proj_id={$other_proj['p_id']}' class='btn default purple'><i class='fa fa-edit'></i> edit </a>":"";
$edit_n_delete_button = $edit_permission?"<a href='javascript:;' role='button' class='btn default btn-xs red delete_other_p'>
								<i class='fa fa-trash-o'></i> delete </a>
							<a href='index.php?page=others_setup&act=2&other_proj_id={$other_proj['p_id']}' class='btn default btn-xs purple'>
								<i class='fa fa-edit'></i> edit </a>":"";

			$output .= "<td>
							<a class='btn default btn-xs blue' data-toggle='modal' href='#basic{$other_proj['p_id']}'>
								<i class='fa fa-share'></i> view </a>

								{$edit_n_delete_button}
						</td>
					</tr>";
					$side_counter++;

			$attachments_output = "no attachments";
			$attachment_set = get_attachment(2, $other_proj['p_id']);
			if (mysql_num_rows($attachment_set)>0) {
			$attachments_output = "
			<div class='gallery row'>
				 <div class='container col-md-12'>
					 <h2>Attachments</h2>
					 <div class='gallery-bottom'>";
					 $i = 0;
					 $flag=true;
				while ( $attachment = mysql_fetch_assoc($attachment_set)) {
					//4 pics in a row (by creating a new gallery-1)
					if ($i % 4 == 0) {
						$attachments_output .=	"<div class='gallery-1'>";
						$flag = true;
					}
					$attachments_output .=	"<div class='col-md-3 gallery-grid'>
										<a class='example-image-link' href='uploads/{$attachment['filename']}' data-lightbox='album_{$other_proj['p_id']}'><img class='example-image' src='uploads/{$attachment['filename']}' alt='image'/></a>
									</div>";
						// $attachments_output .= "<span>i: {$i}, flag: $flag </span>";
					if (++$i % 4 == 0) {
						$attachments_output .= "<div class='clearfix'></div></div>";
						$flag = false;
					}
					// $i++;
				}

				//if <div gallery-1> was opened but not closed then close it. 
				if ($flag) {
					$attachments_output .= "<div class='clearfix'></div></div>";
				}
			$attachments_output .= "</div>
						 </div>
					</div>";

			}

			$branch_modal = is_admin()?"<div class='row'>
									<div class='col-md-4 modal_data'>Name: </div><div class='col-md-8 modal_data'>{$other_proj['branch']}</div>
								</div>":"";

			$modal .= "
				<div class='modal fade' id='basic{$other_proj['p_id']}' tabindex='-1' role='basic' aria-hidden='true'>
				<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='row'>
						<div class='modal-header col-md-12'>
							<h4 class='modal-title'>Patient Information</h4>
						</div>
					</div>
					<div class='row'>
						<div class='modal-body modal-body-scroll col-md-12'>
								
								<div class='row'>
									<div class='col-md-4 modal_data'>Name: </div><div class='col-md-8 modal_data'>{$other_proj['name']}</div>
								</div>
								<div class='row'>
									<div class='col-md-4 modal_data'>Place: </div><div class='col-md-8 modal_data'>{$other_proj['place']}</div>
								</div>
								<div class='row'>
									<div class='col-md-4 modal_data'>Leader: </div><div class='col-md-8 modal_data'>{$other_proj['leader']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Members: </div><div class='col-md-8 modal_data'>" . nl2br($other_proj['members']) . "</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Money: </div><div class='col-md-8 modal_data'>{$other_proj['money']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Date: </div><div class='col-md-8 modal_data'>{$other_proj['date']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Entered By: </div><div class='col-md-8 modal_data'>{$other_proj['entry_user']}</div>
								</div>
									{$branch_modal}
								<div class='row'>
									<div class='col-md-4 modal_data'>Last Updated on: </div><div class='col-md-8 modal_data'>{$other_proj['last_updated']}</div>
								</div>

								<div class='row'>
									<div class='col-md-4 modal_data'>Notes: </div><div class='col-md-8 modal_data'>{$other_proj['note']}</div>
								</div>
								{$attachments_output}
						</div>
					</div>
					<div class='row'>
						<div class='modal-footer col-md-12'>
							<button type='button' class='btn default red' data-dismiss='modal'><i class='fa fa-times-square-o'></i> close</button>
							{$modal_edit}
							
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		";
				}
				echo $output;
				
			?>
		</tbody>
	</table>
	<button class="btn btn-danger tfoot_button">show column search</button>
	</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="index.php?page=others_setup&act=1" class="btn btn-primary"><i class="fa fa-plus-square"></i> add new activity</a>
		</div>
	</div>
	</div>
	
</div>
<?php 
echo $modal;
 ?>

<script src="js/lightbox.js"></script>
<script>
    lightbox.option({
      'positionFromTop': 30
    })
</script>
<script type="text/javascript">

$.fn.dataTableExt.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {
        var iMin = $('#min').val();
        var iMax = $('#max').val();
        d1 = new Date(iMin);
        d2 = new Date(iMax);

        iMin = isNaN(d1.getTime())?"":d1.getTime();
        iMax = isNaN(d2.getTime())?"":d2.getTime();

        target_col_data = aData[6];


        iDate = new Date(target_col_data).getTime();

 		// console.log("iMin = "+iMin);
        // console.log("iMax = "+iMax);
        // console.log("iDate = "+iDate);
        if ( iMin == "" && iMax == "" )
		{
			return true;
		}
		else if ( iMin == "" && iDate < iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && "" == iMax )
		{
			return true;
		}
		else if ( iMin <= iDate && iDate <= iMax )
		{
			return true;
		}
        return false;

    }
);
	$(document).ready(function() {
		
		$(document).on('click', '.tfoot_button', function () {
			$('tfoot').slideToggle();
		});

		$(".errmsj").slideDown(1000);
		$(".alert button.close").click(function (e) {
		    // $(this).parent().fadeOut('slow');
		    $(this).parent().slideUp('slow');
		});

		$('.selection').on('click', '.clickable-row', function(event) {
		  if($(this).hasClass('active-row')){
		    $(this).removeClass('active-row'); 
		  } else {
		    $(this).addClass('active-row').siblings().removeClass('active-row');
		  }
		});

		//number of all columns
    // var numCols = $('#example').DataTable().columns().nodes().length;
	//number of visible columns
    // var numCols = $('#other_proj thead th').length;

    var other_proj = $('#other_proj').DataTable( {
    	processing: true,
    	buttons: [
	        'pdf'
	    ],
    	"pagingType": "full_numbers",
    	"columnDefs": [
		    { "orderable": false, "targets": -1 },
		    { "searchable": false, "targets": -1 }
		  ],
		  "dom": '<"top row"<"col-md-4"l><"rangefilter"><"col-md-4"f>>rt<"bottom row"<"col-md-4"i><"paginationright col-md-8"p>><"clear">',
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "language": {
            "thousands": ","
        },
        "deferRender": true
    } );//end datatable

    $("div.rangefilter").html("<div id='baseDateControl'><div class='col-md-4 dateControlBlock'>Between <input type='text' name='min' id='min' class='date datepick form-control input-sm' value='' size='8' /> and <input type='text' name='max' id='max' class='date datepick form-control input-sm' value='' size='8'/> <a href='javascript:;' class='btn default btn-sm blue' id='clear_search'>clear</a></div></div>");
	
// l - Length changing
// f - Filtering input
// t - The Table!
// i - Information
// p - Pagination
// r - pRocessing

    /*$('#min, #max').keyup( function() {
        other_proj.draw();});*/

    $("#min").keyup ( function() { other_proj.draw(); } );
	$("#min").change( function() { other_proj.draw(); } );
	$("#max").keyup ( function() { other_proj.draw(); } );
	$("#max").change( function() { other_proj.draw(); } );

	$("#clear_search").on("click", function() {
		$('#min').val("");
		$('#max').val("");
		other_proj.draw();
	});

	/*
	pewistm baw initilize a nia chunka classi "date"m bakar hinaya lo inputakan ka xoi initializish 
	dakat w jwan jwanish nishani dadat (taibata baw theme a)
	$('.datepick').datepicker(
			format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true
	);*/	

	 $('#other_proj tfoot th').not(":eq(0),:eq(-1)").each( function () {
        // var title = $.trim($(this).text());
        // console.log($(this));
        // $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search '+$.trim(title)+'" />' );
        $(this).html( '<input type="text" class="form-control input-sm" placeholder="Search here" />' );
        // var disable='';
        // var placeholder = 'placeholder="Search here"';
        // if (title=='tools' || title=='#') { disable="disabled=disabled readonly "; placeholder='style="background-color:transparent; border-color:transparent; display:none"'; }
        	// $(this).html( '<input type="text" '+ disable + ' class="form-control input-sm"'+placeholder+' />' );
    } );
 
    // Apply the search
    other_proj.columns().every( function () {
        var that = this;
 		
 		// console.log(this.index());
        if (this.index()==0 || this.index()==9 ) {return;}
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>