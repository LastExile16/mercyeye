<!-- A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>MercyEye</title>
<meta charset="UTF-8">
<!-- I have added these two -->
<link rel="stylesheet" href="css/bootstrap-3.1.1.min.css">

<link href="css/font-awesome/css/font-awesome.min.css" rel='stylesheet' type='text/css' />
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" href="css/lightbox.css">
<!-- Custom Theme files -->
<link href="css/custom.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="datatables/datatables.min.css"/>
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="css/style4.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />

<!--  below css turns off the round radius -->

<link rel="stylesheet" href="css/datepicker/css/bootstrap-datepicker3.standalone.css">
 

<!-- jQuery (necessary JavaScript plugins) -->
<script src="js/lightbox-plus-jquery.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="css/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="datatables/datatables.min.js"></script>

<!-- Custom Theme files -->

<!-- <script src="js/select2.js"></script> -->
<link rel="stylesheet" type="text/css" href="css/select2.css"/>
<link rel="stylesheet" type="text/css" href="css/select2-bootstrap.css"/>

<!--//theme style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


<style type="text/css">
	/** {
		direction: rtl !important;
	}
	.navbar-nav > li{
		float: right;
	}

	.navbar-nav{
		float: right;
	}

	.navbar-toggle {
    	float: left;
	}
	.copywrite, .copywrite .container, .copywrite .container p{
		direction: ltr !important;
	}*/
	/* Portlet */
	/*.portlet, .search-panel, .student-table, .add-panel{
		float: right;
	}*/

	.contact{
		padding: 2em 0 2em;
	}
	a, a:hover, a:active, a:focus {
	   outline: 0px;
	}
	#logo_a{
		padding: 0;
    	border: 1px solid #E7E7E7;
    	/*_border-width: 0px 3px 3px 3px;*/
	    border-radius: 25%;

background-image: url("./images/logo.jpg");
background-position: -1px -4px;
background-size: 100px 100px;
box-shadow: 0px 0px 8px 2px #867575;
height: 100px;
width: 100px;

    /*_border: 2px solid #ca4242;*/
    /*_border-width: 0px 0px 0px 0px;*/

	}

	#logo_img{
		height: 100px;
	    width: 100px;
	    border-radius: 25%;
	    box-shadow: 0px 0px 8px 2px #867575;
	}
	.logo{
		top: 4px;
	}

	.content.white{
		width: 80%;
		/*background-color: #357;*/
		background-color: #565656;
		color: white;
	}
	.top-menu{
		/*background-color: #357;*/
		background-color: #565656;
		color: white;
	}
	.navbar-default{
		border: 0px;
	}
	ul.nav.navbar-nav li a,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a
	{
    	 padding: 14px 40px;
    	 color: white;
	}
	.top-header{
		padding: 10px 0px;
		background-color: white;
	}
	/*.login-info{
	    float: right;
	    color: white;
	    font-size: small;
	    margin-right: 10px;
	}*/
	ul.nav.navbar-nav li {
	    border-right: 1px solid #333;
	}


	/*top button background colors*/
	.navbar-default .navbar-nav > .active > a, 
	.navbar-default .navbar-nav > .active > a:hover, 
	.navbar-default .navbar-nav > .active > a:focus {
	    color: #fd7364;
	    background-color: black;
	}
	.navbar-default .navbar-nav > .open > a, 
	.navbar-default .navbar-nav > .open > a:focus, 
	.navbar-default .navbar-nav > .open > a:hover li.open,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover

	{
		background-color: black !important;
		color:#fd7364 !important;
	}
	.dropdown-menu {
		background-color: #333;
	}

	.dropdown-menu li a:hover {
		background-color: rgba(0,0,0,0);
	}
	/*end top buttons*/

	.portlet > .portlet-body.green,
	.portlet.green {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	  /*margin-right: 15px;*/
	  /*padding-right: 15px;*/
	  /*padding-left: 15px;*/
	}
	.portlet.box.green > .portlet-title {
	  /*background-color: #35aa47;*/
	  background-color: #73EAC4;
	}
	.portlet.box.green {
	    border: 1px solid #73EAC4;
	}
	table th, table td {
	    /*text-align: right;*/
	    vertical-align: middle;
	}


	.actionbutton{
		float: right;
		margin-right: -10px;
		margin-left: 30px;
	}
	
	.panel-title{
		font-size: 110%;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		color: #000;
	}

	.modal-body-scroll{
		overflow-y: scroll !important;
		max-height: 30em;
	}
	.modal_data{
		border: 1px dashed grey;
		padding: 1% 0% 1% 1%;
	}

	.paginationright {
	    padding-right: 0px;
	}

	.active-row{
		background-color: #E7E7E7 !important; 
	}


	tfoot input {
        width: 100% !important;
        padding: 3px;
        box-sizing: border-box;
    }
    tfoot {
	    /*//display: table-header-group;*/
	}
	.tfoot_button {
		display: none;
	}

	.copywrite p a {
	    color: #AD1212;
	}
	@media screen and (max-width: 1024px) {
	    tfoot {
	        display: none;
	    }
	    .tfoot_button {
	    	display: block;
	    }
	    
	}

	@page { margin: 0; }
</style>

</head>
<body>
<!-- header -->
<div class="top-header">
		 <!-- <div class="login-info"><span>Logged In as <?php echo $_SESSION['fullname']; ?></span> | <span><a href="logout.php">Logout</a></span></div> -->
	 <div class="container">
		 <div class="logo">
			<h1>
				<a id="logo_a" href="index.php">
				 	<img id="logo_img" src="./images/logo.jpg">
				</a>
			</h1>
		 </div>	
	 </div>
</div>
<!---->
<div class="top-menu">
	 <div class="container">
	  <div class="content white">
		 <nav class="navbar navbar-default">
			 <div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>				
			 </div>
			 <!--/.navbar-header-->		
			 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				 <ul class="nav navbar-nav">
				 	<?php 
				 		$page="";
				 		if (isset($_GET['page']) && !empty($_GET['page'])) {
				 			$page = $_GET['page'];
				 		}
				 		else{
							$page = "dashboard";
				 		}

				 	 		
				 	 ?>
					 <li <?php echo $page == "dashboard" ?'class="active"':""; ?> ><a href="index.php?page=dashboard">Dashboard</a></li>
					 <li <?php echo $page == "projects" || $page == "clinics" || $page == "clinics_setup" || $page == "surgery" || $page == "surgery_setup" || $page == "others" || $page == "others_setup"?'class="active"':""; ?> ><a href="index.php?page=projects">Projects</a></li>
					  <!-- setupakan leran wata daxl krdni hospital w disease w ... -->
					 
					 <?php  
					 $active = $page == "branch" || $page == "disease" || $page == "doctor" || $page == "hospital" || $page == "surgery_type" || $page == "users"?'class="active dropdown"':"dropdown";
					 ?>
					 <li <?php echo $active; ?> >
						<a href="index.php?page=system_settings" class="dropdown-toggle" data-toggle="dropdown">System Settings <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<?php echo is_admin()?"<li><a href='index.php?page=branch'>Branch</a></li>":""; ?>
							<li><a href="index.php?page=disease">Disease</a></li>
							<li><a href="index.php?page=doctor">Doctor</a></li>
							<li><a href="index.php?page=hospital">Hospital</a></li>
							<li><a href="index.php?page=surgery_type">Surgery Type</a></li>
							<?php echo is_admin()?"<li><a href='index.php?page=users'>Users</a></li>":""; ?>
						</ul> 
					 </li>
					 <li <?php echo $page == "my_profile"?'class="active dropdown"':"dropdown"; ?> >
					 	<a href="index.php?page=my_profile" class="dropdown-toggle" data-toggle="dropdown">My Profile <b class="caret"></b></a>
						 	<ul class="dropdown-menu">
								<li><a href="index.php?page=my_profile"><?php echo $_SESSION['fullname']; ?></a></li>
								<li><a href="logout.php">Logout</a></li>
							</ul>
					 </li>
					 <!-- <li <?php echo $page == "my_profile"?'class="active"':""; ?> >
					 	<a href="index.php?page=my_profile">My Profile</a>
					 </li> -->
					 
					
					<!-- <li <?php echo $page == "system_settings"?'class="active"':""; ?> >
					 <a href="index.php?page=system_settings">System Settings</a>
					 </li> -->
											
				 </ul>
				</div>
			  <!--/.navbar collapse-->
		 </nav>
		  <!--/.navbar-->		 
	  </div>
	 <div class="clearfix"></div>
		<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
				<!-- <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script> -->

		</div>
</div>