<?php

namespace Halfpastfour\PHPChartJS\Chart;

/**
 * Class HorizontalBar
 * @package Halfpastfour\PHPChartJS\Chart
 */
class HorizontalBar extends Bar
{
	const TYPE = 'horizontalBar';
}