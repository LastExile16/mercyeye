<?php

namespace Halfpastfour\PHPChartJS\Chart;

/**
 * Class Doughnut
 * @package Halfpastfour\PHPChartJS\Chart
 */
class Doughnut extends Pie
{
	const TYPE = 'doughnut';
}