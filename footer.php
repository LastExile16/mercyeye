
<div class="copywrite">
	 <div class="container">
			 <p> © 2016 Nawras. All rights reserved | Developed by <a href="mailto:nawras.nzar@gmail.com">Eng.Nawras Nazar 0750 546 1990</a></p>
	 </div>
</div>

<!---->

<!---->
<!--/animatedcss-->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
<!--script-->
<script type="text/javascript">
		$(document).ready(function() {
				/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
				*/
		$().UItoTop({ easingType: 'easeOutQuart' });

		// disable mousewheel on a input number field when in focus
		// (to prevent Cromium browsers change the value when scrolling)
		$('form').on('focus', 'input[type=number]', function (e) {
		  $(this).on('mousewheel.disableScroll', function (e) {
		    e.preventDefault()
		  })
		})
		$('form').on('blur', 'input[type=number]', function (e) {
		  $(this).off('mousewheel.disableScroll')
		})

		$('.datepick').datepicker({
            format: "yyyy-mm-dd",
		    todayBtn: "linked",
		    autoclose: true,
		    todayHighlight: true
        });

		$("tr:even").css("background-color", "#f8f8f8");

	 	$('.delete_activity').click(function()
		{
			if (confirm(" Are you sure you want to delete this activity"))
			{
				var id = $(this).parent().parent().attr('id');
				var data = 'activity_delete_id=' + id ;
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/delete_activity.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						parent.fadeOut('slow', function() {$(this).remove();});
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});//end delete activity

		$('.delete_other_p').click(function()
		{
			if (confirm(" Are you sure you want to delete this activity"))
			{
				var id = $(this).parent().parent().attr('id');
				var data = 'otherp_delete_id=' + id ;
				var parent = $(this).parent().parent();

				$.ajax(
				{
				   type: "POST",
				   url: "./includes/delete_other_p.php",
				   data: data,
				   cache: false,
				
				   success: function(data1)
				   {
						parent.fadeOut('slow', function() {$(this).remove();});
						//alert(data1);
				   },
				   
				   statusCode: 
				   {
						404: function() {
						alert('ERROR 404 \npage not found');
						}
				   }

				});
			}				
			
		});//end delete other project

});
</script>
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!---->
<script src="js/responsiveslides.min.js"></script> 
</body>
</html>